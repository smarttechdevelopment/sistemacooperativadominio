package com.smartech.sistemacooperativa.dominio;

import java.util.List;

/**
 *
 * @author Smartech
 */
public class Departamento {
    private int id;
    private String nombre;
    private List<Provincia> lstProvincias;

    public Departamento(int id, String nombre, List<Provincia> lstProvincias) {
        this.id = id;
        this.nombre = nombre;
        this.lstProvincias = lstProvincias;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre.toUpperCase();
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Provincia> getLstProvincias() {
        return lstProvincias;
    }

    public void setLstProvincias(List<Provincia> lstProvincias) {
        this.lstProvincias = lstProvincias;
    }
}
