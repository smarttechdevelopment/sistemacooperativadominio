package com.smartech.sistemacooperativa.dominio;

/**
 *
 * @author Smartech
 */
public class DetalleTipoSolicitudTipoUsuario {
    private int id;
    private TipoSolicitud objTipoSolicitud;
    private TipoUsuario objTipoUsuario;

    public DetalleTipoSolicitudTipoUsuario(int id, TipoSolicitud objTipoSolicitud, TipoUsuario objTipoUsuario) {
        this.id = id;
        this.objTipoSolicitud = objTipoSolicitud;
        this.objTipoUsuario = objTipoUsuario;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public TipoSolicitud getObjTipoSolicitud() {
        return objTipoSolicitud;
    }

    public void setObjTipoSolicitud(TipoSolicitud objTipoSolicitud) {
        this.objTipoSolicitud = objTipoSolicitud;
    }

    public TipoUsuario getObjTipoUsuario() {
        return objTipoUsuario;
    }

    public void setObjTipoUsuario(TipoUsuario objTipoUsuario) {
        this.objTipoUsuario = objTipoUsuario;
    }
}
