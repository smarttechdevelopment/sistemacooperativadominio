/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.dominio;

/**
 *
 * @author Smartech
 */
public class Acceso {
    
    public static int ACCESO_SOLICITUD_INGRESO_SOCIO = 1;
    public static int ACCESO_SOLICITUD_PRESTAMO = 2;
    public static int ACCESO_PRESTAMOS = 3;
    public static int ACCESO_RETIROS = 4;
    public static int ACCESO_DEPOSITO = 5;
    private static int ACCESO_TRANSFERENCIAS = 6;
    private static int ACCESO_PAGOS = 7;
    private static int ACCESO_REGISTRO_CUENTA_SOCIO = 8;
    private static int ACCESO_ACTIVAR_TARJETA = 9;
    private static int ACCESO_CAMBIAR_CLAVE_TARJETA = 10;
    private static int ACCESO_REPORTE_SOCIOS = 11;
    
    private int id;
    private String nombre;    
    private String descripcion;

    public Acceso(int id, String nombre, String descripcion) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }    
}
