/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.dominio;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class Deposito implements ITransaccion{
    
    public static Moneda objMonedaBase = new Moneda(0, "Sol", "PEN", "S/", new BigDecimal(1), true);
    public static BigDecimal montoAdvertencia = new BigDecimal(5000);

    private long id;
    private BigDecimal monto;
    private Timestamp fechaRegistro;
    private Timestamp fechaModificacion;
    private boolean estado;
    private Usuario objUsuario;
    private String dniDepositante;
    private boolean habilitado;
    private boolean pagado;
    private Cuenta objCuenta;
    private String acreedor;
    private List<Pago> lstPagos;

    public Deposito(long id, BigDecimal monto, Timestamp fechaRegistro, Timestamp fechaModificacion, boolean estado, Usuario objUsuario, String dniDepositante, boolean habilitado, boolean pagado, Cuenta objCuenta, String acreedor, List<Pago> lstPagos) {
        this.id = id;
        this.monto = monto;
        this.fechaRegistro = fechaRegistro;
        this.fechaModificacion = fechaModificacion;
        this.estado = estado;
        this.objUsuario = objUsuario;
        this.dniDepositante = dniDepositante;
        this.habilitado = habilitado;
        this.pagado = pagado;
        this.objCuenta = objCuenta;
        this.acreedor = acreedor;
        this.lstPagos = lstPagos;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public Timestamp getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Timestamp fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Timestamp getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Timestamp fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public Usuario getObjUsuario() {
        return objUsuario;
    }

    public void setObjUsuario(Usuario objUsuario) {
        this.objUsuario = objUsuario;
    }

    public String getDniDepositante() {
        return dniDepositante;
    }

    public void setDniDepositante(String dniDepositante) {
        this.dniDepositante = dniDepositante;
    }

    public boolean isHabilitado() {
        return habilitado;
    }

    public void setHabilitado(boolean habilitado) {
        this.habilitado = habilitado;
    }

    public boolean isPagado() {
        return pagado;
    }

    public void setPagado(boolean pagado) {
        this.pagado = pagado;
    }

    public Cuenta getObjCuenta() {
        return objCuenta;
    }

    public void setObjCuenta(Cuenta objCuenta) {
        this.objCuenta = objCuenta;
    }

    public String getAcreedor() {
        return acreedor;
    }

    public void setAcreedor(String acreedor) {
        this.acreedor = acreedor;
    }

    public List<Pago> getLstPagos() {
        return lstPagos;
    }

    public void setLstPagos(List<Pago> lstPagos) {
        this.lstPagos = lstPagos;
    }
    
    public static void establecerMontoAdvertencia(Usuario objUsuarioOperador){
        if(objUsuarioOperador.getObjTipoUsuario().getId() < 6){
            montoAdvertencia = new BigDecimal(5000);
        }
    }
    
    public static boolean advertirMontoDeposito(BigDecimal monto, Moneda objMoneda){
        boolean resultado = false;
        
        if(objMonedaBase != null){
            if(objMoneda.getAbreviatura().equals(objMonedaBase.getAbreviatura())){
                if(monto.compareTo(montoAdvertencia) >= 0){
                    resultado = true;
                }
            }else{
                if(objMoneda.getValorPEN(monto).compareTo(objMonedaBase.getValorPEN(montoAdvertencia)) >= 0){
                    resultado = true;
                }
            }
        }
        
        return resultado;
    }
    
    public boolean verificarMontoDepositoYPagos(){        
        return this.getMontoTotalPagos().compareTo(this.monto) >= 0 && (this.monto.compareTo(BigDecimal.ZERO) > 0);
    }
    
    public BigDecimal getMontoTotalPagos(){        
        BigDecimal montoTotal = new BigDecimal(-1);
        
        if(!this.getLstPagos().isEmpty()){
            montoTotal = new BigDecimal(0);
            for(Pago objPago : this.getLstPagos()){
                montoTotal = montoTotal.add(objPago.getMonto());
            }
        }
        return montoTotal;
    }
    
    public boolean actualizarCuenta(){        
        if(this.verificarMontoDepositoYPagos()){
            
            this.getObjCuenta().setSaldo(this.getObjCuenta().getSaldo().add(this.monto));
            return true;
        }
        return false;
    }

    @Override
    public String getDescripcionTransaccion() {
        return "Deposito";
    }

    @Override
    public boolean isIngreso() {
        return true;
    }
}
