package com.smartech.sistemacooperativa.dominio;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 *
 * @author Smartech
 */
public class TransferenciaCuentaEntidadBancaria extends Transferencia{
    
    //True = Cuenta, False = Entidad Bancaria
    private boolean origen;
    private Cuenta objCuenta;
    private EntidadBancaria objEntidadBancaria;

    public TransferenciaCuentaEntidadBancaria(long id, Timestamp fecha, BigDecimal monto, boolean estado, Timestamp fechaRegistro, Moneda objMoneda, Usuario objUsuario, boolean origen, Cuenta objCuenta, EntidadBancaria objEntidadBancaria) {
        super(id, monto, fechaRegistro, fechaRegistro, estado, objUsuario, fecha, objMoneda);
        this.origen = origen;
        this.objCuenta = objCuenta;
        this.objEntidadBancaria = objEntidadBancaria;
    }

    public boolean isOrigen() {
        return origen;
    }

    public void setOrigen(boolean origen) {
        this.origen = origen;
    }

    public Cuenta getObjCuenta() {
        return objCuenta;
    }

    public void setObjCuenta(Cuenta objCuenta) {
        this.objCuenta = objCuenta;
    }

    public EntidadBancaria getObjEntidadBancaria() {
        return objEntidadBancaria;
    }

    public void setObjEntidadBancaria(EntidadBancaria objEntidadBancaria) {
        this.objEntidadBancaria = objEntidadBancaria;
    }
}
