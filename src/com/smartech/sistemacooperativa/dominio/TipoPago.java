/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.dominio;

/**
 *
 * @author Smartech
 */
public class TipoPago {
    
    public static int EFECTIVO = 1;
    public static int DEBITO = 2;
    
    private int id;
    private String nombre;

    public TipoPago() {
        this.id = 0;
        this.nombre = "";
    }

    
    public TipoPago(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre.toUpperCase();
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    
    
}
