package com.smartech.sistemacooperativa.dominio;

import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class SolicitudIngreso extends Solicitud{

    private Socio objSocio;
    private boolean pagado;
    private Timestamp fechaVencimiento;

    public SolicitudIngreso(long id, String codigo, TipoSolicitud objTipoSolicitud, Socio objSocio, boolean aprobado, boolean pagado, boolean estado, Timestamp fechaVencimiento, Timestamp fechaRegistro, Timestamp fechaModificacion, Usuario objUsuario, List<DetalleDocumentoSolicitud> lstDetalleDocumentoSolicitud, List<Aprobacion> lstAprobaciones) {
        super(id, codigo, aprobado, estado, fechaRegistro, fechaModificacion, objUsuario, objTipoSolicitud, lstDetalleDocumentoSolicitud, lstAprobaciones);
        this.objSocio = objSocio;
        this.pagado = pagado;
        this.fechaVencimiento = fechaVencimiento;
    }

    public Socio getObjSocio() {
        return objSocio;
    }

    public void setObjSocio(Socio objSocio) {
        this.objSocio = objSocio;
    }

    public boolean isPagado() {
        return pagado;
    }

    public void setPagado(boolean pagado) {
        this.pagado = pagado;
    }

    public Timestamp getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Timestamp fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }
}
