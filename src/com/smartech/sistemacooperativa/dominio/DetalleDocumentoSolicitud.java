package com.smartech.sistemacooperativa.dominio;

import java.io.InputStream;
import java.sql.Timestamp;

/**
 *
 * @author Smartech
 */
public class DetalleDocumentoSolicitud {
    private long id;
    private Timestamp fechaEntrega;
    private Documento objDocumento;
    private String ruta;
    private InputStream representacion;
    private Solicitud objSolicitud;
    private boolean aprobado;
    private Timestamp fechaModificacion;
    private Timestamp fechaSolicitud;
    private Usuario objUsuario;

    public DetalleDocumentoSolicitud(long id, Timestamp fechaEntrega, Documento objDocumento, String ruta, InputStream representacion, Solicitud objSolicitud, boolean aprobado, Timestamp fechaModificacion, Timestamp fechaSolicitud, Usuario objUsuario) {
        this.id = id;
        this.fechaEntrega = fechaEntrega;
        this.objDocumento = objDocumento;
        this.ruta = ruta;
        this.representacion = representacion;
        this.objSolicitud = objSolicitud;
        this.aprobado = aprobado;
        this.fechaModificacion = fechaModificacion;
        this.fechaSolicitud = fechaSolicitud;
        this.objUsuario = objUsuario;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Timestamp getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(Timestamp fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public Documento getObjDocumento() {
        return objDocumento;
    }

    public void setObjDocumento(Documento objDocumento) {
        this.objDocumento = objDocumento;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public InputStream getRepresentacion() {
        return representacion;
    }

    public void setRepresentacion(InputStream representacion) {
        this.representacion = representacion;
    }

    public Solicitud getObjSolicitud() {
        return objSolicitud;
    }

    public void setObjSolicitud(Solicitud objSolicitud) {
        this.objSolicitud = objSolicitud;
    }

    public boolean isAprobado() {
        return aprobado;
    }

    public void setAprobado(boolean aprobado) {
        this.aprobado = aprobado;
    }

    public Timestamp getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Timestamp fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public Timestamp getFechaSolicitud() {
        return fechaSolicitud;
    }

    public void setFechaSolicitud(Timestamp fechaSolicitud) {
        this.fechaSolicitud = fechaSolicitud;
    }

    public Usuario getObjUsuario() {
        return objUsuario;
    }

    public void setObjUsuario(Usuario objUsuario) {
        this.objUsuario = objUsuario;
    }
}
