package com.smartech.sistemacooperativa.dominio;

import java.util.List;

/**
 *
 * @author Smartech
 */
public class TipoDocumentoIdentidad {
    
    private int id;
    private String nombre;
    private int longitud;
    private boolean permiteLetras;

    public TipoDocumentoIdentidad(){
        this.id = 0;
        this.nombre = "";
        this.longitud = 15;
        this.permiteLetras = true;
    }
    
    public TipoDocumentoIdentidad(int id, String nombre, int longitud, boolean permiteLetras) {
        this.id = id;
        this.nombre = nombre;
        this.longitud = longitud;
        this.permiteLetras = permiteLetras;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre.toUpperCase();
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getLongitud() {
        return longitud;
    }

    public void setLongitud(int longitud) {
        this.longitud = longitud;
    }

    public boolean isPermiteLetras() {
        return permiteLetras;
    }

    public void setPermiteLetras(boolean permiteLetras) {
        this.permiteLetras = permiteLetras;
    }
    
    public static boolean existeLongitud(int longitud, List<TipoDocumentoIdentidad> lstTiposDocumentoIdentidad){
        for (TipoDocumentoIdentidad objTipoDocumentoIdentidad : lstTiposDocumentoIdentidad) {
            if(objTipoDocumentoIdentidad.getLongitud() == longitud){
                return true;
            }
        }
        
        return false;
    }
}
