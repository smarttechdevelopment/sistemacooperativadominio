package com.smartech.sistemacooperativa.dominio;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;

/**
 *
 * @author Smartech
 */
public class DetalleFamiliarSocio {
    
    private int id;
    private String descripcion;
    private BigDecimal porcentajeBeneficio;
    private Socio objSocio;
    private Familiar objFamiliar;
    private Parentesco objParentesco;

    public DetalleFamiliarSocio(int id, String descripcion, BigDecimal porcentajeBeneficio, Socio objSocio, Familiar objFamiliar, Parentesco objParentesco) {
        this.id = id;
        this.descripcion = descripcion;
        this.porcentajeBeneficio = porcentajeBeneficio;
        this.objSocio = objSocio;
        this.objFamiliar = objFamiliar;
        this.objParentesco = objParentesco;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion.toUpperCase();
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public BigDecimal getPorcentajeBeneficio() {
        return porcentajeBeneficio;
    }

    public void setPorcentajeBeneficio(BigDecimal porcentajeBeneficio) {
        this.porcentajeBeneficio = porcentajeBeneficio;
    }

    public Socio getObjSocio() {
        return objSocio;
    }

    public void setObjSocio(Socio objSocio) {
        this.objSocio = objSocio;
    }

    public Familiar getObjFamiliar() {
        return objFamiliar;
    }

    public void setObjFamiliar(Familiar objFamiliar) {
        this.objFamiliar = objFamiliar;
    }

    public Parentesco getObjParentesco() {
        return objParentesco;
    }

    public void setObjParentesco(Parentesco objParentesco) {
        this.objParentesco = objParentesco;
    } 
    
    public BigDecimal getMontoBeneficiario(BigDecimal montoSocio){
        BigDecimal montoBeneficiario = new BigDecimal(BigInteger.ZERO);
        montoBeneficiario = montoBeneficiario.add(montoSocio.multiply(this.porcentajeBeneficio).divide(BigDecimal.valueOf(100), 4, RoundingMode.HALF_UP));
        return montoBeneficiario;
    }
}
