package com.smartech.sistemacooperativa.dominio;

import com.smartech.sistemacooperativa.util.generics.BigDecimalUtil;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 *
 * @author Smartech
 */
public class TasaInteres {

    private int id;
    private String nombre;
    private boolean estado;
    private BigDecimal tasa;
    private BigDecimal tea;
    private BigDecimal tim;
    private BigDecimal montoPlazoFijo;
    private Timestamp fechaRegistro;
    private Timestamp fechaModificacion;
    private TipoInteres objTipoInteres;

    public TasaInteres(int id, String nombre, boolean estado, BigDecimal tasa, BigDecimal tea, BigDecimal tim, BigDecimal montoPlazoFijo, Timestamp fechaRegistro, Timestamp fechaModificacion, TipoInteres objTipoInteres) {
        this.id = id;
        this.nombre = nombre;
        this.estado = estado;
        this.tasa = tasa;
        this.tea = tea;
        this.tim = tim;
        this.montoPlazoFijo = montoPlazoFijo;
        this.fechaRegistro = fechaRegistro;
        this.fechaModificacion = fechaModificacion;
        this.objTipoInteres = objTipoInteres;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre.toUpperCase();
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public BigDecimal getTasa() {
        return BigDecimalUtil.round(tasa);
    }

    public void setTasa(BigDecimal tasa) {
        this.tasa = tasa;
    }

    public BigDecimal getTea() {
        return BigDecimalUtil.round(tea);
    }

    public void setTea(BigDecimal tea) {
        this.tea = tea;
    }

    public BigDecimal getTim() {
        return BigDecimalUtil.round(tim);
    }

    public void setTim(BigDecimal tim) {
        this.tim = tim;
    }

    public BigDecimal getMontoPlazoFijo() {
        return BigDecimalUtil.round(montoPlazoFijo);
    }

    public void setMontoPlazoFijo(BigDecimal montoPlazoFijo) {
        this.montoPlazoFijo = montoPlazoFijo;
    }

    public Timestamp getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Timestamp fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Timestamp getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Timestamp fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public TipoInteres getObjTipoInteres() {
        return objTipoInteres;
    }

    public void setObjTipoInteres(TipoInteres objTipoInteres) {
        this.objTipoInteres = objTipoInteres;
    }
    
    public boolean compareTo(TasaInteres objTasaInteresToCompare){
        return this.nombre.equals(objTasaInteresToCompare.getNombre()) && this.montoPlazoFijo.compareTo(objTasaInteresToCompare.getMontoPlazoFijo()) == 0 && this.tasa.compareTo(objTasaInteresToCompare.getTasa()) == 0 && this.tea.compareTo(objTasaInteresToCompare.getTea()) == 0 && this.tim.compareTo(objTasaInteresToCompare.getTim()) == 0;
    }
}
