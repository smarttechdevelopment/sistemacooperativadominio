package com.smartech.sistemacooperativa.dominio;

/**
 *
 * @author Smartech
 */
public class Documento {
    private int id;
    private String nombre;
    private byte [] formato;
    private String tipoArchivo;

    public Documento(int id, String nombre, byte[] formato, String tipoArchivo) {
        this.id = id;
        this.nombre = nombre;
        this.formato = formato;
        this.tipoArchivo = tipoArchivo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre.toUpperCase();
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public byte[] getFormato() {
        return formato;
    }

    public void setFormato(byte[] formato) {
        this.formato = formato;
    }

    public String getTipoArchivo() {
        return tipoArchivo;
    }

    public void setTipoArchivo(String tipoArchivo) {
        this.tipoArchivo = tipoArchivo;
    }
}
