package com.smartech.sistemacooperativa.dominio;

import java.sql.Timestamp;

/**
 *
 * @author Smartech
 */
public class TasaInteresSolicitudPrestamo {
    
    private long id;
    private Timestamp fechaRegistro;
    private Timestamp fechaModificacion;
    private boolean estado;
    private SolicitudPrestamo objSolicitudPrestamo;
    private TasaInteres objTasaInteres;

    public TasaInteresSolicitudPrestamo(long id, Timestamp fechaRegistro, Timestamp fechaModificacion, boolean estado, SolicitudPrestamo objSolicitudPrestamo, TasaInteres objTasaInteres) {
        this.id = id;
        this.fechaRegistro = fechaRegistro;
        this.fechaModificacion = fechaModificacion;
        this.estado = estado;
        this.objSolicitudPrestamo = objSolicitudPrestamo;
        this.objTasaInteres = objTasaInteres;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Timestamp getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Timestamp fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Timestamp getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Timestamp fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public SolicitudPrestamo getObjSolicitudPrestamo() {
        return objSolicitudPrestamo;
    }

    public void setObjSolicitudPrestamo(SolicitudPrestamo objSolicitudPrestamo) {
        this.objSolicitudPrestamo = objSolicitudPrestamo;
    }
    
    public TasaInteres getObjTasaInteres() {
        return objTasaInteres;
    }

    public void setObjTasaInteres(TasaInteres objTasaInteres) {
        this.objTasaInteres = objTasaInteres;
    }
}
