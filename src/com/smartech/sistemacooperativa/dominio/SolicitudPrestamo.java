package com.smartech.sistemacooperativa.dominio;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class SolicitudPrestamo extends Solicitud implements ITransaccion {

    private BigDecimal otrosIngresos;
    private BigDecimal nivelGastos;
    private int cuotas;
    private String motivo;
    private String motivoDesaprobacion;
    private BigDecimal montoSolicitado;
    private BigDecimal montoAprobado;
    private Timestamp fechaVencimiento;
    private TasaInteres objTasaInteres;
    private TipoPrestamo objTipoPrestamo;
    private List<DetalleSolicitudPrestamoSocio> lstDetalleSolicitudPrestamoSocio;
    private List<Pago> lstPagos;
    private boolean habilitado;
    private boolean pagado;

    public SolicitudPrestamo(long id, String codigo, BigDecimal otrosIngresos, BigDecimal nivelGastos, int cuotas, String motivo, String motivoDesaprobacion, BigDecimal montoSolicitado, BigDecimal montoAprobado, Timestamp fechaVencimiento, Timestamp fechaRegistro, Timestamp fechaModificacion, boolean aprobado, boolean estado, Usuario objUsuario, TipoSolicitud objTipoSolicitud, TasaInteres objTasaInteres, TipoPrestamo objTipoPrestamo, List<DetalleSolicitudPrestamoSocio> lstDetalleSolicitudPrestamoSocio, List<DetalleDocumentoSolicitud> lstDetalleDocumentoSolicitud, List<Aprobacion> lstAprobaciones, List<Pago> lstPagos, boolean habilitado, boolean pagado) {
        super(id, codigo, aprobado, estado, fechaRegistro, fechaModificacion, objUsuario, objTipoSolicitud, lstDetalleDocumentoSolicitud, lstAprobaciones);
        this.otrosIngresos = otrosIngresos;
        this.nivelGastos = nivelGastos;
        this.cuotas = cuotas;
        this.motivo = motivo;
        this.motivoDesaprobacion = motivoDesaprobacion;
        this.montoSolicitado = montoSolicitado;
        this.montoAprobado = montoAprobado;
        this.fechaVencimiento = fechaVencimiento;
        this.objTasaInteres = objTasaInteres;
        this.objTipoPrestamo = objTipoPrestamo;
        this.lstDetalleSolicitudPrestamoSocio = lstDetalleSolicitudPrestamoSocio;
        this.lstPagos = lstPagos;
        this.habilitado = habilitado;
        this.pagado = pagado;
        this.updateLstDetalleSolicitudPrestamoSocio();
    }

    public BigDecimal getOtrosIngresos() {
        return otrosIngresos;
    }

    public void setOtrosIngresos(BigDecimal otrosIngresos) {
        this.otrosIngresos = otrosIngresos;
    }

    public BigDecimal getNivelGastos() {
        return nivelGastos;
    }

    public void setNivelGastos(BigDecimal nivelGastos) {
        this.nivelGastos = nivelGastos;
    }

    public int getCuotas() {
        return cuotas;
    }

    public void setCuotas(int cuotas) {
        this.cuotas = cuotas;
    }

    public String getMotivo() {
        return motivo.toUpperCase();
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getMotivoDesaprobacion() {
        return motivoDesaprobacion.toUpperCase();
    }

    public void setMotivoDesaprobacion(String motivoDesaprobacion) {
        this.motivoDesaprobacion = motivoDesaprobacion;
    }

    public BigDecimal getMontoSolicitado() {
        return montoSolicitado;
    }

    public void setMontoSolicitado(BigDecimal montoSolicitado) {
        this.montoSolicitado = montoSolicitado;
    }

    public BigDecimal getMontoAprobado() {
        return montoAprobado;
    }

    public void setMontoAprobado(BigDecimal montoAprobado) {
        this.montoAprobado = montoAprobado;
    }

    public Timestamp getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Timestamp fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public TasaInteres getObjTasaInteres() {
        return objTasaInteres;
    }

    public void setObjTasaInteres(TasaInteres objTasaInteres) {
        this.objTasaInteres = objTasaInteres;
    }

    public TipoPrestamo getObjTipoPrestamo() {
        return objTipoPrestamo;
    }

    public void setObjTipoPrestamo(TipoPrestamo objTipoPrestamo) {
        this.objTipoPrestamo = objTipoPrestamo;
    }

    public List<DetalleSolicitudPrestamoSocio> getLstDetalleSolicitudPrestamoSocio() {
        return lstDetalleSolicitudPrestamoSocio;
    }

    public void setLstDetalleSolicitudPrestamoSocio(List<DetalleSolicitudPrestamoSocio> lstDetalleSolicitudPrestamoSocio) {
        this.lstDetalleSolicitudPrestamoSocio = lstDetalleSolicitudPrestamoSocio;
        this.updateLstDetalleSolicitudPrestamoSocio();
    }

    public List<Pago> getLstPagos() {
        return lstPagos;
    }

    public void setLstPagos(List<Pago> lstPagos) {
        this.lstPagos = lstPagos;
    }

    public boolean isHabilitado() {
        return habilitado;
    }

    public void setHabilitado(boolean habilitado) {
        this.habilitado = habilitado;
    }

    public boolean isPagado() {
        return pagado;
    }

    public void setPagado(boolean pagado) {
        this.pagado = pagado;
    }

    private void updateLstDetalleSolicitudPrestamoSocio() {
        for (DetalleSolicitudPrestamoSocio objDetalleSolicitudPrestamoSocio : this.lstDetalleSolicitudPrestamoSocio) {
            objDetalleSolicitudPrestamoSocio.setObjSolicitudPrestamo(this);
        }
    }

    public Socio getObjSocioPrestatario() {
        for (DetalleSolicitudPrestamoSocio objDetalleSolicitudPrestamoSocio : this.lstDetalleSolicitudPrestamoSocio) {
            if (!objDetalleSolicitudPrestamoSocio.isGarante()) {
                return objDetalleSolicitudPrestamoSocio.getObjSocio();
            }
        }

        return null;
    }

    public List<DetalleSolicitudPrestamoSocio> getLstDetalleSolicitudPrestamoSocioGarantes() {
        List<DetalleSolicitudPrestamoSocio> lstDetalleSolicitudPrestamoSocioFiadores = new ArrayList<>();

        for (DetalleSolicitudPrestamoSocio objDetalleSolicitudPrestamoSocio : this.lstDetalleSolicitudPrestamoSocio) {
            if (objDetalleSolicitudPrestamoSocio.isGarante()) {
                lstDetalleSolicitudPrestamoSocioFiadores.add(objDetalleSolicitudPrestamoSocio);
            }
        }

        return lstDetalleSolicitudPrestamoSocioFiadores;
    }

    public List<Socio> getLstSociosGarantes() {
        List<Socio> lstSocios = new ArrayList<>();
        List<DetalleSolicitudPrestamoSocio> lstDetalleSolicitudPrestamoSociosFiadores = this.getLstDetalleSolicitudPrestamoSocioGarantes();

        for (DetalleSolicitudPrestamoSocio objDetalleSolicitudPrestamoSocio : lstDetalleSolicitudPrestamoSociosFiadores) {
            lstSocios.add(objDetalleSolicitudPrestamoSocio.getObjSocio());
        }

        return lstSocios;
    }

    public void updateStatus() {
        updateAllDocumentosAprobados();
        if (this.aprobado) {
            this.aprobado = this.montoAprobado.compareTo(BigDecimal.ZERO) > 0 && this.cuotas > 0;
            this.habilitado = this.aprobado;
        }
    }

    @Override
    public BigDecimal getMonto() {
        return this.isAprobado() ? this.montoAprobado : this.montoSolicitado;
    }

    @Override
    public void setMonto(BigDecimal monto) {
        if (this.isAprobado()) {
            this.montoAprobado = monto;
        } else {
            this.montoSolicitado = monto;
        }
    }

    @Override
    public String getDescripcionTransaccion() {
        return "Solicitud de Prestamo";
    }

    @Override
    public boolean isIngreso() {
        return false;
    }
}
