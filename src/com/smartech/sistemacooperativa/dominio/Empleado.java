/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.dominio;

import java.sql.Timestamp;

/**
 *
 * @author Smartech
 */
public class Empleado extends Persona {

    private boolean activo;
    private Distrito objDistrito;
    private GradoInstruccion objGradoInstruccion;
    private Establecimiento objEstablecimiento;
    private Usuario objUsuario;

    public Empleado(long id, String nombre, String apellidoPaterno, String apellidoMaterno, Timestamp fechaNacimiento, String direccion, String DNI, String RUC, String telefono, String celular, boolean sexo, String origen, boolean estado, Timestamp fechaRegistro, Timestamp fechaModificacion, boolean activo, EstadoCivil objEstadoCivil, TipoDocumentoIdentidad objTipoDocumentoIdentidad, Distrito objDistrito, GradoInstruccion objGradoInstruccion, Establecimiento objEstablecimiento, Usuario objUsuario) {
        super(id, null, "", nombre, apellidoPaterno, apellidoMaterno, fechaNacimiento, direccion, DNI, RUC, telefono, celular, sexo, origen, objEstadoCivil, objTipoDocumentoIdentidad, estado, fechaRegistro, fechaModificacion);
        this.activo = activo;
        this.objDistrito = objDistrito;
        this.objGradoInstruccion = objGradoInstruccion;
        this.objEstablecimiento = objEstablecimiento;
        this.objUsuario = objUsuario;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public Distrito getObjDistrito() {
        return objDistrito;
    }

    public void setObjDistrito(Distrito objDistrito) {
        this.objDistrito = objDistrito;
    }

    public GradoInstruccion getObjGradoInstruccion() {
        return objGradoInstruccion;
    }

    public void setObjGradoInstruccion(GradoInstruccion objGradoInstruccion) {
        this.objGradoInstruccion = objGradoInstruccion;
    }

    public Establecimiento getObjEstablecimiento() {
        return objEstablecimiento;
    }

    public void setObjEstablecimiento(Establecimiento objEstablecimiento) {
        this.objEstablecimiento = objEstablecimiento;
    }

    public Usuario getObjUsuario() {
        return objUsuario;
    }

    public void setObjUsuario(Usuario objUsuario) {
        this.objUsuario = objUsuario;
    }
}
