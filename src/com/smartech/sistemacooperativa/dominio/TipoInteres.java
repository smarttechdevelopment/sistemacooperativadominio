package com.smartech.sistemacooperativa.dominio;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class TipoInteres {
    
    private int id;
    private String nombre;
    private int diasPlazos;
    private boolean plazoFijo;
    private boolean estado;
    private Timestamp fechaRegistro;
    private Timestamp fechaModificacion;
    private List<TasaInteres> lstTasasInteres;

    public TipoInteres(int id, String nombre, int diasPlazos, boolean plazoFijo, boolean estado, Timestamp fechaRegistro, Timestamp fechaModificacion, List<TasaInteres> lstTasasInteres) {
        this.id = id;
        this.nombre = nombre;
        this.diasPlazos = diasPlazos;
        this.plazoFijo = plazoFijo;
        this.estado = estado;
        this.fechaRegistro = fechaRegistro;
        this.fechaModificacion = fechaModificacion;
        this.lstTasasInteres = lstTasasInteres;
        this.updateLstTasasInteres();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre.toUpperCase();
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getDiasPlazos() {
        return diasPlazos;
    }

    public void setDiasPlazos(int diasPlazos) {
        this.diasPlazos = diasPlazos;
    }

    public boolean isEstado() {
        return estado;
    }

    public boolean isPlazoFijo() {
        return plazoFijo;
    }

    public void setPlazoFijo(boolean plazoFijo) {
        this.plazoFijo = plazoFijo;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public Timestamp getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Timestamp fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Timestamp getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Timestamp fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public List<TasaInteres> getLstTasasInteres() {
        return lstTasasInteres;
    }

    public void setLstTasasInteres(List<TasaInteres> lstTasasInteres) {
        this.lstTasasInteres = lstTasasInteres;
    }
    
    private void updateLstTasasInteres(){
        for (TasaInteres objTasaInteres : this.lstTasasInteres) {
            objTasaInteres.setObjTipoInteres(this);
        }
    }
    
    public TasaInteres getLastTasaInteres(){
        return this.lstTasasInteres.isEmpty() ? null : this.lstTasasInteres.get(this.lstTasasInteres.size() - 1);
    }
    
    public List<TasaInteres> getLstTasasInteresActivas(){
        List<TasaInteres> lstTasasInteresActivas = new ArrayList<>();
        
        for(TasaInteres objTasaInteres : this.lstTasasInteres){
            if(objTasaInteres.isEstado()){
                lstTasasInteresActivas.add(objTasaInteres);
            }
        }
        
        return lstTasasInteresActivas;
    }
}
