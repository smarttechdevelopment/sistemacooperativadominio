package com.smartech.sistemacooperativa.dominio;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 *
 * @author Smartech
 */
public abstract class Transferencia implements ITransaccion{
    
    private long id;
    private BigDecimal monto;
    private Timestamp fechaRegistro;
    private Timestamp fechaModificacion;
    private boolean estado;
    private Usuario objUsuario;
    protected Timestamp fecha;
    protected Moneda objMoneda;

    public Transferencia(long id, BigDecimal monto, Timestamp fechaRegistro, Timestamp fechaModificacion, boolean estado, Usuario objUsuario, Timestamp fecha, Moneda objMoneda) {
        this.id = id;
        this.monto = monto;
        this.fechaRegistro = fechaRegistro;
        this.fechaModificacion = fechaModificacion;
        this.estado = estado;
        this.objUsuario = objUsuario;
        this.fecha = fecha;
        this.objMoneda = objMoneda;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public Timestamp getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Timestamp fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Timestamp getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Timestamp fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public Usuario getObjUsuario() {
        return objUsuario;
    }

    public void setObjUsuario(Usuario objUsuario) {
        this.objUsuario = objUsuario;
    }

    public Timestamp getFecha() {
        return fecha;
    }

    public void setFecha(Timestamp fecha) {
        this.fecha = fecha;
    }

    public Moneda getObjMoneda() {
        return objMoneda;
    }

    public void setObjMoneda(Moneda objMoneda) {
        this.objMoneda = objMoneda;
    }
    
    @Override
    public String getDescripcionTransaccion(){
        String descripcion = "";
        
        if(this instanceof TransferenciaCuenta){
            TransferenciaCuenta objTransferenciaCuenta = (TransferenciaCuenta) this;
            descripcion = "Transferencia a cuenta " + objTransferenciaCuenta.getObjCuentaDestino().getCodigo();
        }
        
        if(this instanceof TransferenciaCuentaEntidadBancaria){
            
        }
        
        return descripcion;
    }

    @Override
    public boolean isIngreso() {
        return false;
    }
}
