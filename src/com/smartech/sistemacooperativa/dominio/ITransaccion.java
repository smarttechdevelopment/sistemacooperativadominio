package com.smartech.sistemacooperativa.dominio;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 *
 * @author Smartech
 */
public interface ITransaccion {
    
    public static int HABILITADO = 1;
    public static int PAGADO = 2;
    
    public long getId();

    public void setId(long id);

    public BigDecimal getMonto();

    public void setMonto(BigDecimal monto);

    public Timestamp getFechaRegistro();

    public void setFechaRegistro(Timestamp fechaRegistro);

    public Timestamp getFechaModificacion();

    public void setFechaModificacion(Timestamp fechaModificacion);

    public boolean isEstado();

    public void setEstado(boolean estado);

    public Usuario getObjUsuario();

    public void setObjUsuario(Usuario objUsuario);
    
    public String getDescripcionTransaccion();
    
    public boolean isIngreso();
}
