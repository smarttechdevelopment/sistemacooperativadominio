package com.smartech.sistemacooperativa.dominio;

import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class SolicitudRetiroSocio extends Solicitud{

    private String estadoSocio;
    private Socio objSocio;
    private List<DetalleSolicitudRetiroSocio> lstDetalleSolicitudRetiroSocios;
    
    public SolicitudRetiroSocio(long id, String codigo, boolean aprobado, boolean estado, Socio objSocio, String estadoSocio, Timestamp fechaRegistro, Timestamp fechaModificacion, Usuario objUsuario, TipoSolicitud objTipoSolicitud, List<DetalleDocumentoSolicitud> lstDetalleDocumentoSolicitud, List<Aprobacion> lstAprobaciones, List<DetalleSolicitudRetiroSocio> lstDetalleSolicitudRetiroSocios) {
        super(id, codigo, aprobado, estado, fechaRegistro, fechaModificacion, objUsuario, objTipoSolicitud, lstDetalleDocumentoSolicitud, lstAprobaciones);
        this.estadoSocio = estadoSocio;
        this.objSocio = objSocio;
        this.lstDetalleSolicitudRetiroSocios = lstDetalleSolicitudRetiroSocios;
    }

    public String getEstadoSocio() {
        return estadoSocio;
    }

    public void setEstadoSocio(String estadoSocio) {
        this.estadoSocio = estadoSocio;
    }

    public Socio getObjSocio() {
        return objSocio;
    }

    public void setObjSocio(Socio objSocio) {
        this.objSocio = objSocio;
    }

    public List<DetalleSolicitudRetiroSocio> getLstDetalleSolicitudRetiroSocios() {
        return lstDetalleSolicitudRetiroSocios;
    }

    public void setLstDetalleSolicitudRetiroSocios(List<DetalleSolicitudRetiroSocio> lstDetalleSolicitudRetiroSocios) {
        this.lstDetalleSolicitudRetiroSocios = lstDetalleSolicitudRetiroSocios;
    }
    
}
