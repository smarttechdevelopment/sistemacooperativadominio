/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.dominio;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 *
 * @author Smartech
 */
public class Renuncia{
    
    protected long id;
    protected BigDecimal monto;
    protected Timestamp fechaRegistro;
    protected Timestamp fechaModificacion;
    private boolean estado;
    private boolean habilitado;
    private boolean pagado;
    private Pago objPago;
    private Usuario objUsuario; //¿Quién es el usuario que registra y modifica?
    private SolicitudRetiroSocio objSolicitudRetiroSocio;

    public Renuncia(long id, BigDecimal monto, Timestamp fechaRegistro, Timestamp fechaModificacion, boolean estado, boolean habilitado, boolean pagado, Pago objPago, Usuario objUsuario, SolicitudRetiroSocio objSolicitudRetiroSocio) {
        this.id = id;
        this.monto = monto;
        this.fechaRegistro = fechaRegistro;
        this.fechaModificacion = fechaModificacion;
        this.estado = estado;
        this.habilitado = habilitado;
        this.pagado = pagado;
        this.objPago = objPago;
        this.objUsuario = objUsuario;
        this.objSolicitudRetiroSocio = objSolicitudRetiroSocio;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public Timestamp getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Timestamp fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Timestamp getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Timestamp fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public boolean isHabilitado() {
        return habilitado;
    }

    public void setHabilitado(boolean habilitado) {
        this.habilitado = habilitado;
    }

    public boolean isPagado() {
        return pagado;
    }

    public void setPagado(boolean pagado) {
        this.pagado = pagado;
    }

    public Pago getObjPago() {
        return objPago;
    }

    public void setObjPago(Pago objPago) {
        this.objPago = objPago;
    }

    public Usuario getObjUsuario() {
        return objUsuario;
    }

    public void setObjUsuario(Usuario objUsuario) {
        this.objUsuario = objUsuario;
    }

    public SolicitudRetiroSocio getObjSolicitudRetiroSocio() {
        return objSolicitudRetiroSocio;
    }

    public void setObjSolicitudRetiroSocio(SolicitudRetiroSocio objSolicitudRetiroSocio) {
        this.objSolicitudRetiroSocio = objSolicitudRetiroSocio;
    }
    
}
