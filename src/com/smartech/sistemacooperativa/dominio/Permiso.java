package com.smartech.sistemacooperativa.dominio;

/**
 *
 * @author Smartech
 */
public class Permiso {
    
    public static final String PERMISO_SOLICITUD_INGRESO = "SolicitudIngreso";
    public static final String PERMISO_CONSULTA_SOCIOS = "ConsultaSocios";
    public static final String PERMISO_APORTES = "Aportes";
    public static final String PERMISO_DEPOSITOS = "Depositos";
    public static final String PERMISO_RETIROS = "Retiros";
    public static final String PERMISO_TRANSFERENCIAS = "Transferencias";
    public static final String PERMISO_SOLICITUD_PRESTAMO = "SolicitudPrestamo";

    public static final int NIVEL_LECTURA = 1;
    public static final int NIVEL_ESCRITURA = 2;
    public static final int NIVEL_MODIFICACION = 3;
    public static final int NIVEL_ELIMINACION = 4;
    
    // tables
    public static final int PERMISO_DEPOSITOS_ID = 1;
    public static final int PERMISO_RETIROS_ID = 2;
    public static final int PERMISO_DETALLE_SOLICITUD_RETIRO_SOCIO_ID = 3;
    
    private int id;
    private String nombre;
    private int nivel;
    private String descripcion;

    public Permiso(int id, String nombre, String descripcion) {
        this.id = id;
        this.nombre = nombre;
        this.nivel = 0;
        this.descripcion = descripcion;
    }
    
    public Permiso(int id, String nombre, int nivel, String descripcion) {
        this.id = id;
        this.nombre = nombre;
        this.nivel = nivel;
        this.descripcion = descripcion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
