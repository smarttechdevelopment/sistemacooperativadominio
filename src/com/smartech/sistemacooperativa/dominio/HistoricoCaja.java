package com.smartech.sistemacooperativa.dominio;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 *
 * @author Smartech
 */
public class HistoricoCaja {
    
    private long id;
    private Timestamp fechaInicio;
    private Timestamp fechaFin;
    private BigDecimal montoInicio;
    private BigDecimal montoFin;
    private Caja objCaja;
    private Empleado objEmpleado;

    public HistoricoCaja(long id, Timestamp fechaInicio, Timestamp fechaFin, BigDecimal montoInicio, BigDecimal montoFin, Caja objCaja, Empleado objEmpleado) {
        this.id = id;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.montoInicio = montoInicio;
        this.montoFin = montoFin;
        this.objCaja = objCaja;
        this.objEmpleado = objEmpleado;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Timestamp getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Timestamp fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Timestamp getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Timestamp fechaFin) {
        this.fechaFin = fechaFin;
    }

    public BigDecimal getMontoInicio() {
        return montoInicio;
    }

    public void setMontoInicio(BigDecimal montoInicio) {
        this.montoInicio = montoInicio;
    }

    public BigDecimal getMontoFin() {
        return montoFin;
    }

    public void setMontoFin(BigDecimal montoFin) {
        this.montoFin = montoFin;
    }

    public Caja getObjCaja() {
        return objCaja;
    }

    public void setObjCaja(Caja objCaja) {
        this.objCaja = objCaja;
    }

    public Empleado getObjEmpleado() {
        return objEmpleado;
    }

    public void setObjEmpleado(Empleado objEmpleado) {
        this.objEmpleado = objEmpleado;
    }
    
}
