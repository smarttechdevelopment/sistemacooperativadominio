package com.smartech.sistemacooperativa.dominio;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class Comprobante {
    
    private long id;
    private String codigo;
    private String descripcion;
    private BigDecimal monto;
    private boolean ingreso;
    private Timestamp fecha;
    private boolean interesCuenta;
    private Establecimiento objEstablecimiento;
    private List<Pago> lstPagos;

    public Comprobante(long id, String codigo, String descripcion, BigDecimal monto, boolean ingreso, Timestamp fecha, boolean interesCuenta, Establecimiento objEstablecimiento, List<Pago> lstPagos) {
        this.id = id;
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.monto = monto;
        this.ingreso = ingreso;
        this.fecha = fecha;
        this.interesCuenta = interesCuenta;
        this.objEstablecimiento = objEstablecimiento;
        this.lstPagos = lstPagos;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public boolean isIngreso() {
        return ingreso;
    }

    public void setIngreso(boolean ingreso) {
        this.ingreso = ingreso;
    }

    public Timestamp getFecha() {
        return fecha;
    }

    public void setFecha(Timestamp fecha) {
        this.fecha = fecha;
    }

    public boolean isInteresCuenta() {
        return interesCuenta;
    }

    public void setInteresCuenta(boolean interesCuenta) {
        this.interesCuenta = interesCuenta;
    }

    public Establecimiento getObjEstablecimiento() {
        return objEstablecimiento;
    }

    public void setObjEstablecimiento(Establecimiento objEstablecimiento) {
        this.objEstablecimiento = objEstablecimiento;
    }

    public List<Pago> getLstPagos() {
        return lstPagos;
    }

    public void setLstPagos(List<Pago> lstPagos) {
        this.lstPagos = lstPagos;
    }
}
