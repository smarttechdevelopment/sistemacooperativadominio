  package com.smartech.sistemacooperativa.dominio;

import java.util.List;

/**
 *
 * @author Smartech
 */
public class Usuario {
    private long id;
    private String username;
    private String password;
    private boolean estado;
    private boolean activo;
    private TipoUsuario objTipoUsuario;
    private List<Huella> lstHuellas;

    public Usuario(long id, String username, String password, boolean estado, boolean activo, TipoUsuario objTipoUsuario, List<Huella> lstHuellas) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.estado = estado;
        this.activo = activo;
        this.objTipoUsuario = objTipoUsuario;
        this.lstHuellas = lstHuellas;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public TipoUsuario getObjTipoUsuario() {
        return objTipoUsuario;
    }

    public void setObjTipoUsuario(TipoUsuario objTipoUsuario) {
        this.objTipoUsuario = objTipoUsuario;
    }
    
    public boolean verificarAcceso(String nombreAcceso){
        return this.objTipoUsuario.verificarAcceso(nombreAcceso);
    }
    
    public boolean verificarPermiso(String nombrePermiso, int nivelRequerido){
        return this.objTipoUsuario.verificarPermiso(nombrePermiso, nivelRequerido);
    }

    public List<Huella> getLstHuellas() {
        return lstHuellas;
    }

    public void setLstHuellas(List<Huella> lstHuellas) {
        this.lstHuellas = lstHuellas;
    }
}
