package com.smartech.sistemacooperativa.dominio;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class HistoricoCuenta {

    private long id;
    private BigDecimal saldo;
    private Timestamp fecha;
    private Cuenta objCuenta;
    private Comprobante objComprobante;

    public HistoricoCuenta(long id, BigDecimal saldo, Timestamp fecha, Cuenta objCuenta, Comprobante objComprobante) {
        this.id = id;
        this.saldo = saldo;
        this.fecha = fecha;
        this.objCuenta = objCuenta;
        this.objComprobante = objComprobante;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    public Timestamp getFecha() {
        return fecha;
    }

    public void setFecha(Timestamp fecha) {
        this.fecha = fecha;
    }

    public Cuenta getObjCuenta() {
        return objCuenta;
    }

    public void setObjCuenta(Cuenta objCuenta) {
        this.objCuenta = objCuenta;
    }

    public Comprobante getObjComprobante() {
        return objComprobante;
    }

    public void setObjComprobante(Comprobante objComprobante) {
        this.objComprobante = objComprobante;
    }

    public static BigDecimal getTotalDepositosEfectivo(List<HistoricoCuenta> lstHistoricoCuentas) {
        BigDecimal montoTotal = new BigDecimal(BigInteger.ZERO);

        for (HistoricoCuenta objHistoricoCuenta : lstHistoricoCuentas) {
            if (objHistoricoCuenta.getObjComprobante().getDescripcion().equals("Deposito")) {
                if (objHistoricoCuenta.getObjComprobante().getLstPagos().get(0).getObjTipoPago().getNombre().equals("Efectivo")) {
                    montoTotal = montoTotal.add(objHistoricoCuenta.getObjComprobante().getMonto());
                }
            }
        }

        return montoTotal;
    }

    public static BigDecimal getTotalDepositosOtros(List<HistoricoCuenta> lstHistoricoCuentas) {
        BigDecimal montoTotal = new BigDecimal(BigInteger.ZERO);

        for (HistoricoCuenta objHistoricoCuenta : lstHistoricoCuentas) {
            if (objHistoricoCuenta.getObjComprobante().getDescripcion().equals("Deposito")) {
                if (!objHistoricoCuenta.getObjComprobante().getLstPagos().get(0).getObjTipoPago().getNombre().equals("Efectivo")) {
                    montoTotal = montoTotal.add(objHistoricoCuenta.getObjComprobante().getMonto());
                }
            }
        }

        return montoTotal;
    }

    public static BigDecimal getTotalRetiros(List<HistoricoCuenta> lstHistoricoCuentas) {
        BigDecimal montoTotal = new BigDecimal(BigInteger.ZERO);

        for (HistoricoCuenta objHistoricoCuenta : lstHistoricoCuentas) {
            if (objHistoricoCuenta.getObjComprobante().getDescripcion().equals("Retiro")) {
                montoTotal = montoTotal.add(objHistoricoCuenta.getObjComprobante().getMonto());
            }
        }

        return montoTotal;
    }
    
    public static BigDecimal getTotalIntereses(List<HistoricoCuenta> lstHistoricoCuentas){
        BigDecimal montoTotal = new BigDecimal(BigInteger.ZERO);
        
        for(HistoricoCuenta objHistoricoCuenta : lstHistoricoCuentas){
            if(objHistoricoCuenta.getObjComprobante().isInteresCuenta()){
                montoTotal = montoTotal.add(objHistoricoCuenta.getObjComprobante().getMonto());
            }
        }
        
        return montoTotal;
    }
}
