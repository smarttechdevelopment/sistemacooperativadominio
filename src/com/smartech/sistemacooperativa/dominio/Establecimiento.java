package com.smartech.sistemacooperativa.dominio;

/**
 *
 * @author Smartech
 */
public class Establecimiento {
    private int id;
    private String nombre;
    private String codigo;
    private String direccion;
    private Distrito objDistrito;

    public Establecimiento(int id, String nombre, String codigo, String direccion, Distrito objDistrito) {
        this.id = id;
        this.nombre = nombre;
        this.codigo = codigo;
        this.direccion = direccion;
        this.objDistrito = objDistrito;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre.toUpperCase();
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDireccion() {
        return direccion.toUpperCase();
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Distrito getObjDistrito() {
        return objDistrito;
    }

    public void setObjDistrito(Distrito objDistrito) {
        this.objDistrito = objDistrito;
    }
}
