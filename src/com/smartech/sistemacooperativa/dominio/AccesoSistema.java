package com.smartech.sistemacooperativa.dominio;

import java.sql.Timestamp;

/**
 *
 * @author Smartech
 */
public class AccesoSistema {
    
    private long id;
    private Timestamp fecha;
    private Usuario objUsuario;

    public AccesoSistema(long id, Timestamp fecha, Usuario objUsuario) {
        this.id = id;
        this.fecha = fecha;
        this.objUsuario = objUsuario;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Timestamp getFecha() {
        return fecha;
    }

    public void setFecha(Timestamp fecha) {
        this.fecha = fecha;
    }

    public Usuario getObjUsuario() {
        return objUsuario;
    }

    public void setObjUsuario(Usuario objUsuario) {
        this.objUsuario = objUsuario;
    }
}
