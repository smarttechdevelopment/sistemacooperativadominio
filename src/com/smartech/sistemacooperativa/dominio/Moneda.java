package com.smartech.sistemacooperativa.dominio;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 *
 * @author Smartech
 */
public class Moneda {
    
    private int id;
    private String nombre;
    private String abreviatura;
    private String simbolo;
    private BigDecimal valorActual;
    private boolean base;

    public Moneda(){
        this.id = 0;
        this.nombre = "";
        this.abreviatura = "";
        this.simbolo = "";
        this.valorActual = new BigDecimal(BigInteger.ZERO);
        this.base = false;
    }
    
    public Moneda(int id, String nombre, String abreviatura, String simbolo, BigDecimal valorActual, boolean base) {
        this.id = id;
        this.nombre = nombre;
        this.abreviatura = abreviatura;
        this.simbolo = simbolo;
        this.valorActual = valorActual;
        this.base = base;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre.toUpperCase();
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAbreviatura() {
        return abreviatura.toUpperCase();
    }

    public void setAbreviatura(String abreviatura) {
        this.abreviatura = abreviatura;
    }

    public String getSimbolo() {
        return simbolo;
    }

    public void setSimbolo(String simbolo) {
        this.simbolo = simbolo;
    }

    public BigDecimal getValorActual() {
        return valorActual;
    }

    public void setValorActual(BigDecimal valorActual) {
        this.valorActual = valorActual;
    }

    public boolean isBase() {
        return base;
    }

    public void setBase(boolean base) {
        this.base = base;
    }
    
    public boolean equalsTo(Moneda objMoneda){
        return this.getId() == objMoneda.getId();
    }
    
    public BigDecimal getValorPEN(BigDecimal monto){        
        return monto.multiply(getValorActual());
    }
    
}
