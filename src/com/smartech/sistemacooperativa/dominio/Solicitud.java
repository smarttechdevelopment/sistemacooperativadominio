package com.smartech.sistemacooperativa.dominio;

import java.io.File;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Smartech
 */
public abstract class Solicitud {

    protected long id;
    protected String codigo;
    protected boolean aprobado;
    protected boolean estado;
    protected Timestamp fechaRegistro;
    protected Timestamp fechaModificacion;
    protected Usuario objUsuario;
    protected TipoSolicitud objTipoSolicitud;
    protected List<DetalleDocumentoSolicitud> lstDetalleDocumentoSolicitud;
    protected List<Aprobacion> lstAprobaciones;

    public Solicitud(long id, String codigo, boolean aprobado, boolean estado, Timestamp fechaRegistro, Timestamp fechaModificacion, Usuario objUsuario, TipoSolicitud objTipoSolicitud, List<DetalleDocumentoSolicitud> lstDetalleDocumentoSolicitud, List<Aprobacion> lstAprobaciones) {
        this.id = id;
        this.codigo = codigo;
        this.aprobado = aprobado;
        this.estado = estado;
        this.fechaRegistro = fechaRegistro;
        this.fechaModificacion = fechaModificacion;
        this.objUsuario = objUsuario;
        this.objTipoSolicitud = objTipoSolicitud;
        this.lstDetalleDocumentoSolicitud = lstDetalleDocumentoSolicitud;
        this.lstAprobaciones = lstAprobaciones;
        this.updateLstDetalleDocumentoSolicitud();
        this.updateLstAprobaciones();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public boolean isAprobado() {
        return aprobado;
    }

    public void setAprobado(boolean aprobado) {
        this.aprobado = aprobado;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public Timestamp getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Timestamp fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Timestamp getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Timestamp fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public Usuario getObjUsuario() {
        return objUsuario;
    }

    public void setObjUsuario(Usuario objUsuario) {
        this.objUsuario = objUsuario;
    }

    public TipoSolicitud getObjTipoSolicitud() {
        return objTipoSolicitud;
    }

    public void setObjTipoSolicitud(TipoSolicitud objTipoSolicitud) {
        this.objTipoSolicitud = objTipoSolicitud;
    }

    public List<DetalleDocumentoSolicitud> getLstDetalleDocumentoSolicitud() {
        return lstDetalleDocumentoSolicitud;
    }

    public void setLstDetalleDocumentoSolicitud(List<DetalleDocumentoSolicitud> lstDetalleDocumentoSolicitud) {
        this.lstDetalleDocumentoSolicitud = lstDetalleDocumentoSolicitud;
        this.updateLstDetalleDocumentoSolicitud();
    }

    public List<Aprobacion> getLstAprobaciones() {
        return lstAprobaciones;
    }

    public void setLstAprobaciones(List<Aprobacion> lstAprobaciones) {
        this.lstAprobaciones = lstAprobaciones;
        this.updateLstAprobaciones();
    }

    protected void updateLstDetalleDocumentoSolicitud() {
        for (DetalleDocumentoSolicitud objDetalleDocumentoSolicitud : this.lstDetalleDocumentoSolicitud) {
            objDetalleDocumentoSolicitud.setObjSolicitud(this);
        }
    }

    protected void updateLstAprobaciones() {
        for (Aprobacion objAprobacion : this.lstAprobaciones) {
            objAprobacion.setObjSolicitud(this);
        }
    }

    protected boolean checkAprobaciones() {
        for (Aprobacion objAprobacion : this.lstAprobaciones) {
            if (!objAprobacion.isAprobado()) {
                return false;
            }
        }

        return true;
    }

    public void updateAllDocumentosAprobados() {
        for (DetalleDocumentoSolicitud objDetalleDocumentoSolicitud : this.lstDetalleDocumentoSolicitud) {
            if (!objDetalleDocumentoSolicitud.isAprobado()) {
                this.aprobado = false;
                return;
            }
        }

        this.aprobado = this.checkAprobaciones();
    }

    public String getAprobacionTableName() {
        String table = "";

        if (this instanceof SolicitudIngreso) {
            table = "aprobacionesingreso";
        }
        if (this instanceof SolicitudPrestamo) {
            table = "aprobacionesprestamo";
        }
        if (this instanceof SolicitudRetiroSocio) {
            table = "aprobacionesretirosocio";
        }

        return table;
    }

    public String getForeignKeyName() {
        String foreignKey = "";

        if (this instanceof SolicitudIngreso) {
            foreignKey = "idsolicitudingreso";
        }
        if (this instanceof SolicitudPrestamo) {
            foreignKey = "idsolicitudprestamo";
        }
        if (this instanceof SolicitudRetiroSocio) {
            foreignKey = "idsolicitudretirosocio";
        }

        return foreignKey;
    }

    public String getDetalleDocumentoTableName() {
        String table = "";

        if (this instanceof SolicitudIngreso) {
            table = "detalledocumentosolicitudingreso";
        }
        if (this instanceof SolicitudPrestamo) {
            table = "detalledocumentosolicitudprestamo";
        }
        if (this instanceof SolicitudRetiroSocio) {
            table = "detalledocumentosolicitudretirosocio";
        }

        return table;
    }
    
    public void updateRutas(Map<Documento, File> mapArchivos, String ruta){
        for(DetalleDocumentoSolicitud objDetalleDocumentoSolicitud : this.lstDetalleDocumentoSolicitud){
            File file = mapArchivos.get(objDetalleDocumentoSolicitud.getObjDocumento());
            String nuevaRuta = file == null ? objDetalleDocumentoSolicitud.getRuta() : ruta.concat("/" + file.getName());
            objDetalleDocumentoSolicitud.setRuta(nuevaRuta); 
        }
    }
}
