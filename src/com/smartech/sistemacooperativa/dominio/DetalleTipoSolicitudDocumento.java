package com.smartech.sistemacooperativa.dominio;

/**
 *
 * @author Smartech
 */
public class DetalleTipoSolicitudDocumento {
    private int id;
    private boolean estado;
    private boolean activo;
    private TipoSolicitud objTipoSolicitud;
    private Documento objDocumento;
    private Establecimiento objEstablecimiento;

    public DetalleTipoSolicitudDocumento(int id, boolean estado, boolean activo, TipoSolicitud objTipoSolicitud, Documento objDocumento, Establecimiento objEstablecimiento) {
        this.id = id;
        this.estado = estado;
        this.activo = activo;
        this.objTipoSolicitud = objTipoSolicitud;
        this.objDocumento = objDocumento;
        this.objEstablecimiento = objEstablecimiento;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public TipoSolicitud getObjTipoSolicitud() {
        return objTipoSolicitud;
    }

    public void setObjTipoSolicitud(TipoSolicitud objTipoSolicitud) {
        this.objTipoSolicitud = objTipoSolicitud;
    }

    public Documento getObjDocumento() {
        return objDocumento;
    }

    public void setObjDocumento(Documento objDocumento) {
        this.objDocumento = objDocumento;
    }

    public Establecimiento getObjEstablecimiento() {
        return objEstablecimiento;
    }

    public void setObjEstablecimiento(Establecimiento objEstablecimiento) {
        this.objEstablecimiento = objEstablecimiento;
    }
}
