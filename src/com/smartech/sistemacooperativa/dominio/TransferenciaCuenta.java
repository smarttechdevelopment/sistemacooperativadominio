package com.smartech.sistemacooperativa.dominio;

import com.smartech.sistemacooperativa.util.generics.DateUtil;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 *
 * @author Smartech
 */
public class TransferenciaCuenta extends Transferencia {

    private Cuenta objCuentaOrigen;
    private Cuenta objCuentaDestino;

    public TransferenciaCuenta(long id, Timestamp fecha, BigDecimal monto, boolean estado, Timestamp fechaRegistro, Moneda objMoneda, Usuario objUsuario, Cuenta objCuentaOrigen, Cuenta objCuentaDestino) {
        super(id, monto, fechaRegistro, fechaRegistro, estado, objUsuario, fecha, objMoneda);
        this.objCuentaOrigen = objCuentaOrigen;
        this.objCuentaDestino = objCuentaDestino;
    }

    public Cuenta getObjCuentaOrigen() {
        return objCuentaOrigen;
    }

    public void setObjCuentaOrigen(Cuenta objCuentaOrigen) {
        this.objCuentaOrigen = objCuentaOrigen;
    }

    public Cuenta getObjCuentaDestino() {
        return objCuentaDestino;
    }

    public void setObjCuentaDestino(Cuenta objCuentaDestino) {
        this.objCuentaDestino = objCuentaDestino;
    }

    public boolean evaluarMontoCuentas() {
        boolean result;
        result = this.getObjCuentaOrigen().isEstado() && this.getObjCuentaOrigen().isActivo();
        if (result) {
            result = this.getObjCuentaDestino().isEstado() && this.getObjCuentaDestino().isActivo();
            if (result) {
                return this.getObjCuentaOrigen().getSaldo().compareTo(this.getMonto()) > -1;
            }
        }
        return false;
    }

    public boolean actualizarCuentas() {

        if (this.evaluarMontoCuentas()) {

            this.getObjCuentaOrigen().setSaldo(this.getObjCuentaOrigen().getSaldo().subtract(super.getMonto()));
            this.getObjCuentaOrigen().setFechamodificacion(DateUtil.currentTimestamp());
            this.getObjCuentaDestino().setSaldo(this.getObjCuentaDestino().getSaldo().add(super.getMonto()));
            this.getObjCuentaDestino().setFechamodificacion(DateUtil.currentTimestamp());

            this.setFechaModificacion(DateUtil.currentTimestamp());
            return true;
        }
        return false;
    }
}
