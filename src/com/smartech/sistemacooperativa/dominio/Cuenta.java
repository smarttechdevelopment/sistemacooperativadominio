package com.smartech.sistemacooperativa.dominio;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class Cuenta {
    
    public static int DIGITOS_CODIGO = 12;
    
    private long id;
    private String codigo;
    private BigDecimal saldo;
    private Timestamp fecharegistro;
    private Timestamp fechamodificacion;
    private boolean estado;
    private boolean activo;
    private Empleado objEmpleado;
    private TasaInteres objTasaInteres;
    private TipoCuenta objTipoCuenta;
    private Usuario objUsuario;
    private List<Socio> lstSocios;
    private boolean bloqueadoEntrada;
    private boolean bloqueadoSalida;

    public Cuenta(long id, String codigo, BigDecimal saldo, Timestamp fecharegistro, Timestamp fechamodificacion, boolean estado, boolean activo, Empleado objEmpleado, TasaInteres objTasaInteres, TipoCuenta objTipoCuenta, Usuario objUsuario, List<Socio> lstSocios, boolean bloqueadoEntrada, boolean bloqueadoSalida) {
        this.id = id;
        this.codigo = codigo;
        this.saldo = saldo;
        this.fecharegistro = fecharegistro;
        this.fechamodificacion = fechamodificacion;
        this.estado = estado;
        this.activo = activo;
        this.objEmpleado = objEmpleado;
        this.objTasaInteres = objTasaInteres;
        this.objTipoCuenta = objTipoCuenta;
        this.objUsuario = objUsuario;
        this.lstSocios = lstSocios;
        this.bloqueadoEntrada = bloqueadoEntrada;
        this.bloqueadoSalida = bloqueadoSalida;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    public Timestamp getFecharegistro() {
        return fecharegistro;
    }

    public void setFecharegistro(Timestamp fecharegistro) {
        this.fecharegistro = fecharegistro;
    }

    public Timestamp getFechamodificacion() {
        return fechamodificacion;
    }

    public void setFechamodificacion(Timestamp fechamodificacion) {
        this.fechamodificacion = fechamodificacion;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public Empleado getObjEmpleado() {
        return objEmpleado;
    }

    public void setObjEmpleado(Empleado objEmpleado) {
        this.objEmpleado = objEmpleado;
    }

    public TasaInteres getObjTasaInteres() {
        return objTasaInteres;
    }

    public void setObjTasaInteres(TasaInteres objTasaInteres) {
        this.objTasaInteres = objTasaInteres;
    }

    public TipoCuenta getObjTipoCuenta() {
        return objTipoCuenta;
    }

    public void setObjTipoCuenta(TipoCuenta objTipoCuenta) {
        this.objTipoCuenta = objTipoCuenta;
    }

    public Usuario getObjUsuario() {
        return objUsuario;
    }

    public void setObjUsuario(Usuario objUsuario) {
        this.objUsuario = objUsuario;
    }

    public List<Socio> getLstSocios() {
        return lstSocios;
    }

    public void setLstSocios(List<Socio> lstSocios) {
        this.lstSocios = lstSocios;
    }

    public boolean isBloqueadoEntrada() {
        return bloqueadoEntrada;
    }

    public void setBloqueadoEntrada(boolean bloqueadoEntrada) {
        this.bloqueadoEntrada = bloqueadoEntrada;
    }

    public boolean isBloqueadoSalida() {
        return bloqueadoSalida;
    }

    public void setBloqueadoSalida(boolean bloqueadoSalida) {
        this.bloqueadoSalida = bloqueadoSalida;
    }
    
    public String getNombreCliente(){
        String nombre = "";
        
        int size = this.lstSocios.size();
        for(int i = 0; i < size; i++){
            if(i != size - 1){
                nombre += lstSocios.get(i).getNombreCompleto() + ", ";
            }else{
                nombre += lstSocios.get(i).getNombreCompleto();
            }
        }
        
        return nombre;
    }
    
    public String getCodigoCliente(){
        String codigo = "";
        
        int size = this.lstSocios.size();
        for(int i = 0; i < size; i++){
            if(i != size - 1){
                codigo += lstSocios.get(i).getCodigo()+ ", ";
            }else{
                codigo += lstSocios.get(i).getCodigo();
            }
        }
        
        return codigo;
    }
    
    public String getDireccionCliente(){
        return this.lstSocios.size() > 1 ? "-" : this.lstSocios.get(0).getDireccion();
    }
    
    public boolean verificarSocio(Socio objSocio){
        for(Socio objSocioCuenta : this.lstSocios){
            if(objSocioCuenta.getId() == objSocio.getId()){
                return true;
            }
        }
        
        return false;
    }
}
