package com.smartech.sistemacooperativa.dominio;

import java.awt.image.BufferedImage;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author Smartech
 */
public class Socio extends Persona {  
    public static int MESES_LIMITE_INACTIVIDAD = 0;
    public static int PERIODO_MESES_FONDO_MORTUORIO = 0;
    public static String ESTADO_ACTIVIDAD_ACTIVO = "ACTIVO";
    public static String ESTADO_ACTIVIDAD_PASIVO = "PASIVO";
    
    private String codigo;
    private String ocupacion;
    private GradoInstruccion objGradoInstruccion;
    private String direccionLaboral;
    private BigDecimal ingresoMensual;
    private Timestamp fechaIngreso;
    private boolean activo;
    private boolean juridico;
    private boolean renunciado;
    private Distrito objDistrito;
    private Usuario objUsuario;
    private TipoTrabajador objTipoTrabajador;
    private Tarjeta objTarjeta;
    private List<Cuenta> lstCuentas;
    private List<DetalleFamiliarSocio> lstDetalleFamiliarSocio;

    public Socio(long id, BufferedImage foto, String rutaFoto, String nombre, String apellidoPaterno, String apellidoMaterno, Timestamp fechaNacimiento, String direccion, String DNI, String RUC, String telefono, String celular, boolean sexo, String origen, boolean estado, Timestamp fechaIngreso, Timestamp fechaRegistro, Timestamp fechaModificacion, String codigo, String ocupacion, String direccionLaboral, BigDecimal ingresoMensual, boolean activo, boolean juridico, boolean renunciado, Distrito objDistrito, Usuario objUsuario, TipoTrabajador objTipoTrabajador, Tarjeta objTarjeta, GradoInstruccion objGradoInstruccion, EstadoCivil objEstadoCivil, TipoDocumentoIdentidad objTipoDocumentoIdentidad, List<Cuenta> lstCuentas, List<DetalleFamiliarSocio> lstDetalleFamiliarSocio) {
        super(id, foto, rutaFoto, nombre, apellidoPaterno, apellidoMaterno, fechaNacimiento, direccion, DNI, RUC, telefono, celular, sexo, origen, objEstadoCivil, objTipoDocumentoIdentidad, estado, fechaRegistro, fechaModificacion);
        this.codigo = codigo;
        this.fechaIngreso = fechaIngreso;
        this.ocupacion = ocupacion;
        this.direccionLaboral = direccionLaboral;
        this.ingresoMensual = ingresoMensual;
        this.ingresoMensual = ingresoMensual;
        this.activo = activo;
        this.juridico = juridico;
        this.renunciado = renunciado;
        this.objDistrito = objDistrito;
        this.objUsuario = objUsuario;
        this.objTipoTrabajador = objTipoTrabajador;
        this.objTarjeta = objTarjeta;
        this.objGradoInstruccion = objGradoInstruccion;
        this.lstCuentas = lstCuentas;
        this.lstDetalleFamiliarSocio = lstDetalleFamiliarSocio;
        this.setSocioToLstDetalleFamiliarSocio();
    }    

    public String getOcupacion() {
        return ocupacion.toUpperCase();
    }

    public void setOcupacion(String ocupacion) {
        this.ocupacion = ocupacion;
    }

    public GradoInstruccion getObjGradoInstruccion() {
        return objGradoInstruccion;
    }

    public void setObjGradoInstruccion(GradoInstruccion objGradoInstruccion) {
        this.objGradoInstruccion = objGradoInstruccion;
    }

    public List<DetalleFamiliarSocio> getLstDetalleFamiliarSocio() {
        return lstDetalleFamiliarSocio;
    }

    public void setLstDetalleFamiliarSocio(List<DetalleFamiliarSocio> lstDetalleFamiliarSocio) {
        this.lstDetalleFamiliarSocio = lstDetalleFamiliarSocio;
        this.setSocioToLstDetalleFamiliarSocio();
    }

    public String getDireccionLaboral() {
        return direccionLaboral.toUpperCase();
    }

    public void setDireccionLaboral(String direccionLaboral) {
        this.direccionLaboral = direccionLaboral;
    }

    public BigDecimal getIngresoMensual() {
        return ingresoMensual;
    }

    public void setIngresoMensual(BigDecimal ingresoMensual) {
        this.ingresoMensual = ingresoMensual;
    }

    public Timestamp getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Timestamp fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public boolean isJuridico() {
        return juridico;
    }

    public void setJuridico(boolean juridico) {
        this.juridico = juridico;
    }

    public boolean isRenunciado() {
        return renunciado;
    }

    public void setRenunciado(boolean renunciado) {
        this.renunciado = renunciado;
    }

    public Distrito getObjDistrito() {
        return objDistrito;
    }

    public void setObjDistrito(Distrito objDistrito) {
        this.objDistrito = objDistrito;
    }

    public Usuario getObjUsuario() {
        return objUsuario;
    }

    public void setObjUsuario(Usuario objUsuario) {
        this.objUsuario = objUsuario;
    }

    public TipoTrabajador getObjTipoTrabajador() {
        return objTipoTrabajador;
    }

    public void setObjTipoTrabajador(TipoTrabajador objTipoTrabajador) {
        this.objTipoTrabajador = objTipoTrabajador;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Tarjeta getObjTarjeta() {
        return objTarjeta;
    }

    public void setObjTarjeta(Tarjeta objTarjeta) {
        this.objTarjeta = objTarjeta;
    }

    public List<Cuenta> getLstCuentas() {
        return lstCuentas;
    }

    public void setLstCuentas(List<Cuenta> lstCuentas) {
        this.lstCuentas = lstCuentas;
    }

    public List<DetalleFamiliarSocio> getLstFamiliaresSocio() {
        return lstDetalleFamiliarSocio;
    }
    
    private void setSocioToLstDetalleFamiliarSocio(){
        for(DetalleFamiliarSocio objDetalleFamiliarSocio : this.lstDetalleFamiliarSocio){
            objDetalleFamiliarSocio.setObjSocio(this);
        }
    }
    
    public List<Cuenta> getCuentas(boolean bloqueoEntrada, boolean bloqueoSalida){
        List<Cuenta> lstCuentasReturn = new ArrayList<>();
        
        for (Cuenta objCuenta : this.lstCuentas) {
            if(objCuenta.isBloqueadoEntrada() == bloqueoEntrada && objCuenta.isBloqueadoSalida() == bloqueoSalida){
                lstCuentasReturn.add(objCuenta);
            }
        }
        
        return lstCuentasReturn;
    }
}
