package com.smartech.sistemacooperativa.dominio;

import java.math.BigDecimal;

/**
 *
 * @author Smartech
 */
public class Caja {
    private int id;
    private BigDecimal monto;
    private String codigo;
    private Establecimiento objEstablecimiento;
    private boolean abierto;

    public Caja(int id, BigDecimal monto, String codigo, Establecimiento objEstablecimiento, boolean abierto) {
        this.id = id;
        this.monto = monto;
        this.codigo = codigo;
        this.objEstablecimiento = objEstablecimiento;
        this.abierto = abierto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Establecimiento getObjEstablecimiento() {
        return objEstablecimiento;
    }

    public void setObjEstablecimiento(Establecimiento objEstablecimiento) {
        this.objEstablecimiento = objEstablecimiento;
    }

    public boolean isAbierto() {
        return abierto;
    }

    public void setAbierto(boolean abierto) {
        this.abierto = abierto;
    }
}
