package com.smartech.sistemacooperativa.dominio;

/**
 *
 * @author Smartech
 */
public class Aprobacion {
    
    private long id;
    private boolean aprobado;
    private Solicitud objSolicitud;
    private TipoUsuario objTipoUsuario;
    private Usuario objUsuario;

    public Aprobacion(long id, boolean aprobado, Solicitud objSolicitud, TipoUsuario objTipoUsuario, Usuario objUsuario) {
        this.id = id;
        this.aprobado = aprobado;
        this.objSolicitud = objSolicitud;
        this.objTipoUsuario = objTipoUsuario;
        this.objUsuario = objUsuario;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isAprobado() {
        return aprobado;
    }

    public void setAprobado(boolean aprobado) {
        this.aprobado = aprobado;
    }

    public Solicitud getObjSolicitud() {
        return objSolicitud;
    }

    public void setObjSolicitud(Solicitud objSolicitud) {
        this.objSolicitud = objSolicitud;
    }

    public TipoUsuario getObjTipoUsuario() {
        return objTipoUsuario;
    }

    public void setObjTipoUsuario(TipoUsuario objTipoUsuario) {
        this.objTipoUsuario = objTipoUsuario;
    }

    public Usuario getObjUsuario() {
        return objUsuario;
    }

    public void setObjUsuario(Usuario objUsuario) {
        this.objUsuario = objUsuario;
    }
}
