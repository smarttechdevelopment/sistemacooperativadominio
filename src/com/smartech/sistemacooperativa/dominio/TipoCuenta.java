package com.smartech.sistemacooperativa.dominio;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class TipoCuenta {
    public static int TIPO_AHORRO_LIBRE = 1;
    public static int TIPO_MANCOMUNADA_CONJUNTA = 3;
    public static int TIPO_MANCOMUNADA_INDISTINTA = 4;
    public static int TIPO_FONDO_MORTUORIO = 5;
    
    private int id;
    private String nombre;
    private Moneda objMoneda;
    private List<TipoInteres> lstTiposInteres;
    private boolean mancomunada;
    private boolean registroManual;
    
    public TipoCuenta(int id, String nombre, Moneda objMoneda, boolean mancomunada, boolean registroManual) {
        this.id = id;
        this.nombre = nombre;
        this.objMoneda = objMoneda;
        this.lstTiposInteres = new ArrayList<>();
        this.mancomunada = mancomunada;
        this.registroManual = registroManual;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre.toUpperCase();
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Moneda getObjMoneda() {
        return objMoneda;
    }

    public void setObjMoneda(Moneda objMoneda) {
        this.objMoneda = objMoneda;
    }

    public List<TipoInteres> getLstTiposInteres() {
        return lstTiposInteres;
    }

    public void setLstTiposInteres(List<TipoInteres> lstTiposInteres) {
        this.lstTiposInteres = lstTiposInteres;
    }

    public boolean isMancomunada() {
        return mancomunada;
    }

    public void setMancomunada(boolean mancomunada) {
        this.mancomunada = mancomunada;
    }

    public boolean isRegistroManual() {
        return registroManual;
    }

    public void setRegistroManual(boolean registroManual) {
        this.registroManual = registroManual;
    }

    public boolean verificacionUnanime(){             
        if(this.getId() == TIPO_MANCOMUNADA_CONJUNTA || this.getId() == TIPO_AHORRO_LIBRE){
            return true;
        }
        return false;
    }
}
