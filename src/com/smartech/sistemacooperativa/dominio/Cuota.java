package com.smartech.sistemacooperativa.dominio;

import com.smartech.sistemacooperativa.util.generics.DateUtil;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.Date;

/**
 *
 * @author Smartech
 */
public class Cuota {

    private long id;
    private BigDecimal monto;
    private int numero;
    private BigDecimal interes;
    private BigDecimal amortizacion;
    private BigDecimal mora;
    private BigDecimal deudaPendiente;
    private Timestamp fechaVencimiento;
    private Timestamp fechaPago;
    private boolean pagado;
    private Prestamo objPrestamo;
    private Timestamp fechaRegistro;
    private Timestamp fechaModificacion;
    private boolean estado;
    private Usuario objUsuario;

    public Cuota(long id, BigDecimal monto, int numero, BigDecimal interes, BigDecimal amortizacion, BigDecimal mora, BigDecimal deudaPendiente, Timestamp fechaVencimiento, Timestamp fechaPago, boolean pagado, Timestamp fechaRegistro, boolean estado, Usuario objUsuario, Prestamo objPrestamo) {
        this.id = id;
        this.monto = monto;
        this.numero = numero;
        this.interes = interes;
        this.amortizacion = amortizacion;
        this.mora = mora;
        this.deudaPendiente = deudaPendiente;
        this.fechaVencimiento = fechaVencimiento;
        this.fechaPago = fechaPago;
        this.pagado = pagado;
        this.objPrestamo = objPrestamo;
        this.fechaRegistro = fechaRegistro;
        this.fechaModificacion = DateUtil.currentTimestamp();
        this.estado = estado;
        this.objUsuario = objUsuario;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public BigDecimal getInteres() {
        return interes;
    }

    public void setInteres(BigDecimal interes) {
        this.interes = interes;
    }

    public BigDecimal getAmortizacion() {
        return amortizacion;
    }

    public void setAmortizacion(BigDecimal amortizacion) {
        this.amortizacion = amortizacion;
    }

    public BigDecimal getMora() {
        return mora;
    }

    public void setMora(BigDecimal mora) {
        this.mora = mora;
    }

    public BigDecimal getDeudaPendiente() {
        return deudaPendiente;
    }

    public void setDeudaPendiente(BigDecimal deudaPendiente) {
        this.deudaPendiente = deudaPendiente;
    }

    public Timestamp getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Timestamp fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public Timestamp getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(Timestamp fechaPago) {
        this.fechaPago = fechaPago;
    }

    public boolean isPagado() {
        return pagado;
    }

    public void setPagado(boolean pagado) {
        this.pagado = pagado;
    }

    public Prestamo getObjPrestamo() {
        return objPrestamo;
    }

    public void setObjPrestamo(Prestamo objPrestamo) {
        this.objPrestamo = objPrestamo;
    }

    public Timestamp getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Timestamp fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Timestamp getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Timestamp fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public Usuario getObjUsuario() {
        return objUsuario;
    }

    public void setObjUsuario(Usuario objUsuario) {
        this.objUsuario = objUsuario;
    }

    public BigDecimal calcularInteresCompensatorioVencido(Date fechaCalculo) {
        BigDecimal interesCompensatorioVencido = new BigDecimal(BigInteger.ZERO);

        try {
            int diasRetraso = DateUtil.getDaysDifference(DateUtil.getDate(this.fechaVencimiento), fechaCalculo);
            if (diasRetraso > 0) {
                BigDecimal base = BigDecimal.ONE.add(this.objPrestamo.getTed());
                BigDecimal primerFactor = BigDecimal.valueOf(Math.pow(base.doubleValue(), diasRetraso)).subtract(BigDecimal.ONE);
                BigDecimal segundoFactor = this.monto.add(this.interes);

                interesCompensatorioVencido = primerFactor.multiply(segundoFactor);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return interesCompensatorioVencido;
    }

    public BigDecimal calcularTasaMoratoriaDiario() {
        BigDecimal tasaMoratoriaDiaria = new BigDecimal(BigInteger.ZERO);

        try {
            BigDecimal potencia = BigDecimal.ONE.divide(BigDecimal.valueOf(360), 4, RoundingMode.HALF_UP);
            BigDecimal base = BigDecimal.ONE.add(this.objPrestamo.getObjSolicitudPrestamo().getObjTasaInteres().getTim());

            tasaMoratoriaDiaria = BigDecimal.valueOf(Math.pow(base.doubleValue(), potencia.doubleValue())).subtract(BigDecimal.ONE);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tasaMoratoriaDiaria;
    }

    public BigDecimal calcularInteresMoratorio(Date fechaCalculo) {
        BigDecimal interesMoratorio = new BigDecimal(BigInteger.ZERO);

        try {
            if (this.fechaVencimiento.before(DateUtil.getTimestamp(fechaCalculo))) {
                BigDecimal base = BigDecimal.ONE.add(this.calcularTasaMoratoriaDiario());
                BigDecimal potencia = BigDecimal.valueOf(DateUtil.getDaysDifference(this.fechaVencimiento, DateUtil.currentTimestamp()));

                interesMoratorio = (BigDecimal.valueOf(Math.pow(base.doubleValue(), potencia.doubleValue())).subtract(BigDecimal.ONE)).multiply(this.monto);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return interesMoratorio;
    }

    public BigDecimal calcularMonto(Date fechaCalculo) {
        BigDecimal montoCuota = new BigDecimal(BigInteger.ZERO);

        try {
            montoCuota = this.monto.add(this.calcularInteresCompensatorioVencido(fechaCalculo)).add(this.calcularInteresMoratorio(fechaCalculo));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return montoCuota;
    }

    public void updateMontos(Date fechaCalculo) {
        this.interes = this.interes.add(this.calcularInteresCompensatorioVencido(fechaCalculo));
        this.mora = this.calcularInteresMoratorio(fechaCalculo);
    }
    
    public void updateMontoTotal(){
        this.monto = this.amortizacion.add(this.interes).add(this.mora);
    }

    public String getEstado(Date fechaCalculo) {
        return this.pagado ? "Pagado" : this.getDiasRetraso(fechaCalculo) > 0 ? "Vencido" : "Pendiente";
    }

    public int getDiasRetraso(Date fechaCalculo) {
        int diasRetraso = DateUtil.getDaysDifference(DateUtil.getDate(this.fechaVencimiento), fechaCalculo);
        return diasRetraso > 0 ? diasRetraso : 0;
    }
}
