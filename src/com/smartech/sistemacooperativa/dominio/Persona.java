package com.smartech.sistemacooperativa.dominio;

import java.awt.image.BufferedImage;
import java.io.File;
import java.sql.Timestamp;

/**
 *
 * @author Smartech
 */
public abstract class Persona {
    protected long id;
    protected BufferedImage foto;
    protected String rutaFoto;
    protected String nombre;
    protected String apellidoPaterno;
    protected String apellidoMaterno;
    protected Timestamp fechaNacimiento;
    protected String direccion;
    protected String documentoIdentidad;
    protected String RUC;
    protected String telefono;
    protected String celular;
    protected boolean sexo;
    protected String origen;
    protected EstadoCivil objEstadoCivil;
    protected TipoDocumentoIdentidad objTipoDocumentoIdentidad;
    protected boolean estado;
    protected Timestamp fechaRegistro;
    protected Timestamp fechaModificacion;

    public Persona(long id, BufferedImage foto, String rutaFoto, String nombre, String apellidoPaterno, String apellidoMaterno, Timestamp fechaNacimiento, String direccion, String documentoIdentidad, String RUC, String telefono, String celular, boolean sexo, String origen, EstadoCivil objEstadoCivil, TipoDocumentoIdentidad objTipoDocumentoIdentidad, boolean estado, Timestamp fechaRegistro, Timestamp fechaModificacion) {
        this.id = id;
        this.foto = foto;
        this.rutaFoto = rutaFoto;
        this.nombre = nombre;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.fechaNacimiento = fechaNacimiento;
        this.direccion = direccion;
        this.documentoIdentidad = documentoIdentidad;
        this.RUC = RUC;
        this.telefono = telefono;
        this.celular = celular;
        this.sexo = sexo;
        this.origen = origen;
        this.objEstadoCivil = objEstadoCivil;
        this.estado = estado;
        this.fechaRegistro = fechaRegistro;
        this.fechaModificacion = fechaModificacion;
        this.objTipoDocumentoIdentidad = objTipoDocumentoIdentidad;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BufferedImage getFoto() {
        return foto;
    }

    public void setFoto(BufferedImage foto) {
        this.foto = foto;
    }

    public String getRutaFoto() {
        return rutaFoto;
    }

    public void setRutaFoto(String rutaFoto) {
        this.rutaFoto = rutaFoto;
    }

    public String getNombre() {
        return this.nombre == null ? "" : nombre.toUpperCase();
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoPaterno() {
        return this.apellidoPaterno == null ? "" : apellidoPaterno.toUpperCase();
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return this.apellidoMaterno == null ? "" : apellidoMaterno.toUpperCase();
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public Timestamp getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Timestamp fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getDireccion() {
        return direccion == null ? "" : direccion.toUpperCase();
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDocumentoIdentidad() {
        return documentoIdentidad == null ? "" : documentoIdentidad.toUpperCase();
    }

    public void setDocumentoIdentidad(String documentoIdentidad) {
        this.documentoIdentidad = documentoIdentidad;
    }

    public String getRUC() {
        return RUC;
    }

    public void setRUC(String RUC) {
        this.RUC = RUC;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public boolean isSexo() {
        return sexo;
    }

    public void setSexo(boolean sexo) {
        this.sexo = sexo;
    }

    public String getOrigen() {
        return origen == null ? "" : origen.toUpperCase();
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public EstadoCivil getObjEstadoCivil() {
        return objEstadoCivil;
    }

    public void setObjEstadoCivil(EstadoCivil objEstadoCivil) {
        this.objEstadoCivil = objEstadoCivil;
    }

    public TipoDocumentoIdentidad getObjTipoDocumentoIdentidad() {
        return objTipoDocumentoIdentidad;
    }

    public void setObjTipoDocumentoIdentidad(TipoDocumentoIdentidad objTipoDocumentoIdentidad) {
        this.objTipoDocumentoIdentidad = objTipoDocumentoIdentidad;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public Timestamp getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Timestamp fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Timestamp getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Timestamp fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }
    
    public String getNombreCompleto(){
        String nombreCompleto = this.getApellidoPaterno() + " " + this.getApellidoMaterno() + ", " + this.getNombre();
        return nombreCompleto;
    }
    
    public String getNombreCompletoUpLowCase(){ // no se usa pero NO BORRAR
        String nombrecompleto = this.getNombreCompleto();
        nombrecompleto = nombrecompleto.toLowerCase();
        String[] array = nombrecompleto.split("\\s");
        
        String fullname = "";
        
        for(String objString : array){
            String uno = String.valueOf(objString.charAt(0));
            String resto = objString.substring(1, objString.length());
            objString = uno.toUpperCase().concat(resto);
            fullname = fullname.concat(objString + " ");
        }
        
        return fullname;
    }
    
    public void updateRutaFoto(File fileFoto, String ruta){
        if(fileFoto != null){
            this.rutaFoto = ruta.concat("/" + fileFoto.getName());
        }
    }
}
