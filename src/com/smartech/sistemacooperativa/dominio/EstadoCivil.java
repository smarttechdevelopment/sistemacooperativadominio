package com.smartech.sistemacooperativa.dominio;

/**
 *
 * @author Smartech
 */
public class EstadoCivil {
    
    private int id;
    private String nombre;

    public EstadoCivil() {
        this.id = 0;
        this.nombre = "";
    }

    public EstadoCivil(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre.toUpperCase();
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
