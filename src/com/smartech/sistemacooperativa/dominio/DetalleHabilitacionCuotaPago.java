package com.smartech.sistemacooperativa.dominio;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 *
 * @author Smartech
 */
public class DetalleHabilitacionCuotaPago {

    private long id;
    private BigDecimal monto;
    private Timestamp fechaRegistro;
    private Timestamp fechaModificacion;
    private boolean estado;
    private Pago objPago;
}
