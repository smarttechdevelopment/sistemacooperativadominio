package com.smartech.sistemacooperativa.dominio;

/**
 *
 * @author Smartech
 */
public class Distrito {
    private int id;
    private String nombre;
    private Provincia objProvincia;

    public Distrito(int id, String nombre, Provincia objProvincia) {
        this.id = id;
        this.nombre = nombre;
        this.objProvincia = objProvincia;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre.toUpperCase();
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Provincia getObjProvincia() {
        return objProvincia;
    }

    public void setObjProvincia(Provincia objProvincia) {
        this.objProvincia = objProvincia;
    }
    
    public String getUbigeo(){
        return this.nombre.toUpperCase() + ". " + this.getObjProvincia().getNombre() + ". " + this.getObjProvincia().getObjDepartamento().getNombre();
    }
}
