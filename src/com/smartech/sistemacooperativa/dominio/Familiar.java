/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.dominio;

import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class Familiar extends Persona {

    private List<DetalleFamiliarSocio> lstFamiliaresSocio;

    public Familiar(long id, String nombre, String apellidoPaterno, String apellidoMaterno, Timestamp fechaNacimiento, String direccion, String DNI, String RUC, String telefono, String celular, boolean sexo, String origen, boolean estado, Timestamp fechaRegistro, Timestamp fechaModificacion, EstadoCivil objEstadoCivil, TipoDocumentoIdentidad objTipoDocumentoIdentidad, List<DetalleFamiliarSocio> lstFamiliaresSocio) {
        super(id, null, "", nombre, apellidoPaterno, apellidoMaterno, fechaNacimiento, direccion, DNI, RUC, telefono, celular, sexo, origen, objEstadoCivil, objTipoDocumentoIdentidad, estado, fechaRegistro, fechaModificacion);
        this.lstFamiliaresSocio = lstFamiliaresSocio;
    }

    public List<DetalleFamiliarSocio> getLstFamiliaresSocio() {
        return lstFamiliaresSocio;
    }

    public void setLstFamiliaresSocio(List<DetalleFamiliarSocio> lstFamiliaresSocio) {
        this.lstFamiliaresSocio = lstFamiliaresSocio;
    }
}
