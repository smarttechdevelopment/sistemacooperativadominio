package com.smartech.sistemacooperativa.dominio;

import com.smartech.sistemacooperativa.util.generics.StringUtil;
import java.sql.Timestamp;

/**
 *
 * @author Smartech
 */
public class Tarjeta {

    public static int DIGITOS_TARJETA = 12;
    public static int MINUTOS_BLOQUEO_TARJETA = 1;
    
    private long id;
    private String codigo;
    private String pin;
    private Timestamp fechaEntrega;
    private boolean activo;
    private int intentos;
    private Timestamp fechaRegistro;
    private Timestamp fechaModificacion;
    private Timestamp fechaBloqueoIntentos;
    private boolean estado;
    private TipoTarjeta objTipoTarjeta;
    private Socio objSocio;

    public Tarjeta(long id, String codigo, String pin, Timestamp fechaEntrega, boolean activo, int intentos, Timestamp fechaRegistro, Timestamp fechaModificacion, Timestamp fechaBloqueoIntentos, boolean estado, TipoTarjeta objTipoTarjeta, Socio objSocio) {
        this.id = id;
        this.codigo = codigo;
        this.pin = pin;
        this.fechaEntrega = fechaEntrega;
        this.activo = activo;
        this.intentos = intentos;
        this.fechaRegistro = fechaRegistro;
        this.fechaModificacion = fechaModificacion;
        this.fechaBloqueoIntentos = fechaBloqueoIntentos;
        this.estado = estado;
        this.objTipoTarjeta = objTipoTarjeta;
        this.objSocio = objSocio;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public Timestamp getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(Timestamp fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public int getIntentos() {
        return intentos;
    }

    public void setIntentos(int intentos) {
        this.intentos = intentos;
    }

    public Timestamp getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Timestamp fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Timestamp getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Timestamp fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public Timestamp getFechaBloqueoIntentos() {
        return fechaBloqueoIntentos;
    }

    public void setFechaBloqueoIntentos(Timestamp fechaBloqueoIntentos) {
        this.fechaBloqueoIntentos = fechaBloqueoIntentos;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public TipoTarjeta getObjTipoTarjeta() {
        return objTipoTarjeta;
    }

    public void setObjTipoTarjeta(TipoTarjeta objTipoTarjeta) {
        this.objTipoTarjeta = objTipoTarjeta;
    }

    public Socio getObjSocio() {
        return objSocio;
    }

    public void setObjSocio(Socio objSocio) {
        this.objSocio = objSocio;
    }

    public Socio getObjSocio(boolean estado){
        if(getObjSocio().isEstado() == estado){
            return getObjSocio();
        }
        return null;
    }
    
    public static String getNumeroTarjeta(String numeroTarjeta) {
        return numeroTarjeta.substring(1, DIGITOS_TARJETA + 1);
    }

    public String getCodigoEncriptado() {
        return StringUtil.getAsterisks(DIGITOS_TARJETA - 4) + this.codigo.substring(this.codigo.length() - 4, this.codigo.length());
    }

    public static String getNumeroTarjetaEncriptado(String numeroTarjeta) {
        if (numeroTarjeta.length() <= DIGITOS_TARJETA - 4) {
            return StringUtil.getAsterisks(numeroTarjeta.length());
        }
        String suffix = numeroTarjeta.substring(DIGITOS_TARJETA - 4, numeroTarjeta.length());
        return StringUtil.getAsterisks(DIGITOS_TARJETA - 4) + suffix;
    }
    
    public static boolean isValido(String string) {
        return doLuhn(string, false) % 10 == 0;
    }

    public static String generarDigitoVerificacion(String string) {
        int digit = 10 - doLuhn(string, true) % 10;
        return "" + digit;
    }
    
    private static int doLuhn(String s, boolean evenPosition) {
        int sum = 0;
        for (int i = s.length() - 1; i >= 0; i--) {
            int n = Integer.parseInt(s.substring(i, i + 1));
            if (evenPosition) {
                n *= 2;
                if (n > 9) {
                    n = (n % 10) + 1;
                }
            }
            sum += n;
            evenPosition = !evenPosition;
        }

        return sum;
    }
}
