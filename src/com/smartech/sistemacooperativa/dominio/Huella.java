package com.smartech.sistemacooperativa.dominio;

/**
 *
 * @author Smartech
 */
public class Huella {
    
    private long id;
    private byte[] huella;
    private String dedo;
    private boolean mano;

    public Huella(long id, byte[] huella, String dedo, boolean mano) {
        this.id = id;
        this.huella = huella;
        this.dedo = dedo;
        this.mano = mano;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public byte[] getHuella() {
        return huella;
    }

    public void setHuella(byte[] huella) {
        this.huella = huella;
    }

    public String getDedo() {
        return dedo;
    }

    public void setDedo(String dedo) {
        this.dedo = dedo;
    }

    public boolean isMano() {
        return mano;
    }

    public void setMano(boolean mano) {
        this.mano = mano;
    }
}
