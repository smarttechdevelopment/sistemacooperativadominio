package com.smartech.sistemacooperativa.dominio;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class Pago {
    
    private long id;
    private Timestamp fecha;
    private BigDecimal monto;
    private Timestamp fechaRegistro;
    private Timestamp fechaModificacion;
    private boolean estado;
    private TipoPago objTipoPago;
    private Concepto objConcepto;
    private Moneda objMoneda;

    public Pago() {        
        this.id = 0;
        this.fecha = null;
        this.monto = new BigDecimal(0);
        this.fechaRegistro = null;
        this.fechaModificacion = null;
        this.estado = false;
        this.objTipoPago = new TipoPago();
        this.objConcepto = new Concepto();
        this.objMoneda = new Moneda();
    }

    public Pago(long id, Timestamp fecha, BigDecimal monto, Timestamp fechaRegistro, Timestamp fechaModificacion, boolean estado, TipoPago objTipoPago, Concepto objConcepto, Moneda objMoneda) {
        this.id = id;
        this.fecha = fecha;
        this.monto = monto;
        this.fechaRegistro = fechaRegistro;
        this.fechaModificacion = fechaModificacion;
        this.estado = estado;
        this.objTipoPago = objTipoPago;
        this.objConcepto = objConcepto;
        this.objMoneda = objMoneda;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Timestamp getFecha() {
        return fecha;
    }

    public void setFecha(Timestamp fecha) {
        this.fecha = fecha;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public Timestamp getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Timestamp fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Timestamp getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Timestamp fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public TipoPago getObjTipoPago() {
        return objTipoPago;
    }

    public void setObjTipoPago(TipoPago objTipoPago) {
        this.objTipoPago = objTipoPago;
    }

    public Concepto getObjConcepto() {
        return objConcepto;
    }

    public void setObjConcepto(Concepto objConcepto) {
        this.objConcepto = objConcepto;
    }

    public Moneda getObjMoneda() {
        return objMoneda;
    }

    public void setObjMoneda(Moneda objMoneda) {
        this.objMoneda = objMoneda;
    }
    
    public static BigDecimal getMontoTotal(List<Pago> lstPagos) {
        BigDecimal montoTotal = new BigDecimal(BigInteger.ZERO);

        for (Pago objPago : lstPagos) {
            montoTotal = montoTotal.add(objPago.getMonto());
        }

        return montoTotal;
    }
}
