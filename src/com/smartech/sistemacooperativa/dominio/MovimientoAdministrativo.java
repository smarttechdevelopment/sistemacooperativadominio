package com.smartech.sistemacooperativa.dominio;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class MovimientoAdministrativo implements ITransaccion {

    private long id;
    private BigDecimal monto;
    private Timestamp fechaRegistro;
    private Timestamp fechaModificacion;
    private boolean estado;
    private Usuario objUsuario;
    private String descripcion;
    private boolean ingreso;
    private boolean habilitado;
    private boolean pagado;
    private Empleado objEmpleado;
    private List<Pago> lstPagos;

    public MovimientoAdministrativo(long id, BigDecimal monto, Timestamp fechaRegistro, Timestamp fechaModificacion, boolean estado, Usuario objUsuario, String descripcion, boolean ingreso, boolean habilitado, boolean pagado, Empleado objEmpleado, List<Pago> lstPagos) {
        this.id = id;
        this.monto = monto;
        this.fechaRegistro = fechaRegistro;
        this.fechaModificacion = fechaModificacion;
        this.estado = estado;
        this.objUsuario = objUsuario;
        this.descripcion = descripcion;
        this.ingreso = ingreso;
        this.habilitado = habilitado;
        this.pagado = pagado;
        this.objEmpleado = objEmpleado;
        this.lstPagos = lstPagos;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public Timestamp getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Timestamp fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Timestamp getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Timestamp fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public Usuario getObjUsuario() {
        return objUsuario;
    }

    public void setObjUsuario(Usuario objUsuario) {
        this.objUsuario = objUsuario;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public boolean isIngreso() {
        return ingreso;
    }

    public void setIngreso(boolean ingreso) {
        this.ingreso = ingreso;
    }

    public boolean isHabilitado() {
        return habilitado;
    }

    public void setHabilitado(boolean habilitado) {
        this.habilitado = habilitado;
    }

    public boolean isPagado() {
        return pagado;
    }

    public void setPagado(boolean pagado) {
        this.pagado = pagado;
    }

    public Empleado getObjEmpleado() {
        return objEmpleado;
    }

    public void setObjEmpleado(Empleado objEmpleado) {
        this.objEmpleado = objEmpleado;
    }

    public List<Pago> getLstPagos() {
        return lstPagos;
    }

    public void setLstPagos(List<Pago> lstPagos) {
        this.lstPagos = lstPagos;
    }

    @Override
    public String getDescripcionTransaccion() {
        return this.ingreso ? "Ingreso caja administrativo" : "Egreso caja administrativo";
    }
}
