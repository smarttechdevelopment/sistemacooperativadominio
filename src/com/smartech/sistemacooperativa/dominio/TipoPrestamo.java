package com.smartech.sistemacooperativa.dominio;

/**
 *
 * @author Smartech
 */
public class TipoPrestamo {
    
    private int id;
    private String nombre;
    private Moneda objMoneda;

    public TipoPrestamo(int id, String nombre, Moneda objMoneda) {
        this.id = id;
        this.nombre = nombre;
        this.objMoneda = objMoneda;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre.toUpperCase();
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Moneda getObjMoneda() {
        return objMoneda;
    }

    public void setObjMoneda(Moneda objMoneda) {
        this.objMoneda = objMoneda;
    }
}
