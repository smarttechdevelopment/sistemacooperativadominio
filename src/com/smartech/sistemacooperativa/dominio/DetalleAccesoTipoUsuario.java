package com.smartech.sistemacooperativa.dominio;

/**
 *
 * @author Smartech
 */
public class DetalleAccesoTipoUsuario {
    private long id;
    private TipoUsuario objTipoUsuario;
    private Acceso objAcceso;

    public DetalleAccesoTipoUsuario(long id, TipoUsuario objTipoUsuario, Acceso objAcceso) {
        this.id = id;
        this.objTipoUsuario = objTipoUsuario;
        this.objAcceso = objAcceso;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public TipoUsuario getObjTipoUsuario() {
        return objTipoUsuario;
    }

    public void setObjTipoUsuario(TipoUsuario objTipoUsuario) {
        this.objTipoUsuario = objTipoUsuario;
    }

    public Acceso getObjAcceso() {
        return objAcceso;
    }

    public void setObjAcceso(Acceso objAcceso) {
        this.objAcceso = objAcceso;
    }
}
