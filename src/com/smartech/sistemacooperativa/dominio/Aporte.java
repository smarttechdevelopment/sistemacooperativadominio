package com.smartech.sistemacooperativa.dominio;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class Aporte implements ITransaccion{

    public static BigDecimal PAGO_APORTE_SOLES;
    public static BigDecimal PAGO_APORTE_FONDO_MORTUORIO_SOLES;
    public static Moneda MONEDA_BASE;//= new Moneda(0, "Sol", "PEN", "S/", new BigDecimal(1), true);
    
    private long id;
    private BigDecimal monto;
    private Timestamp fechaRegistro;
    private Timestamp fechaModificacion;
    private boolean estado;
    private Usuario objUsuario;
    private boolean habilitado;
    private boolean pagado;
    private boolean ingreso;
    private boolean retiro;
    private Tarjeta objTarjeta;
    private List<Pago> lstPagos;

    public Aporte(long id, BigDecimal monto, Timestamp fechaRegistro, Timestamp fechaModificacion, boolean estado, Usuario objUsuario, boolean habilitado, boolean pagado, boolean ingreso, boolean retiro, Tarjeta objTarjeta, List<Pago> lstPagos) {
        this.id = id;
        this.monto = monto;
        this.fechaRegistro = fechaRegistro;
        this.fechaModificacion = fechaModificacion;
        this.estado = estado;
        this.objUsuario = objUsuario;
        this.habilitado = habilitado;
        this.pagado = pagado;
        this.ingreso = ingreso;
        this.retiro = retiro;
        this.objTarjeta = objTarjeta;
        this.lstPagos = lstPagos;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public Timestamp getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Timestamp fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Timestamp getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Timestamp fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public Usuario getObjUsuario() {
        return objUsuario;
    }

    public void setObjUsuario(Usuario objUsuario) {
        this.objUsuario = objUsuario;
    }

    public boolean isHabilitado() {
        return habilitado;
    }

    public void setHabilitado(boolean habilitado) {
        this.habilitado = habilitado;
    }

    public boolean isPagado() {
        return pagado;
    }

    public void setPagado(boolean pagado) {
        this.pagado = pagado;
    }

    public boolean isIngreso() {
        return ingreso;
    }

    public void setIngreso(boolean ingreso) {
        this.ingreso = ingreso;
    }

    public boolean isRetiro() {
        return retiro;
    }

    public void setRetiro(boolean retiro) {
        this.retiro = retiro;
    }

    public Tarjeta getObjTarjeta() {
        return objTarjeta;
    }

    public void setObjTarjeta(Tarjeta objTarjeta) {
        this.objTarjeta = objTarjeta;
    }

    public List<Pago> getLstPagos() {
        return lstPagos;
    }

    public void setLstPagos(List<Pago> lstPagos) {
        this.lstPagos = lstPagos;
    }

    @Override
    public String getDescripcionTransaccion() {
        return "Aporte";
    }
}
