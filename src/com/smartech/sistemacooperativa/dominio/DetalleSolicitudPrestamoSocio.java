package com.smartech.sistemacooperativa.dominio;

import java.math.BigDecimal;

/**
 *
 * @author Smartech
 */
public class DetalleSolicitudPrestamoSocio {
    
    private long id;
    private boolean garante;
    private BigDecimal ingresos;
    private BigDecimal egresos;
    private BigDecimal otrosIngresos;
    private BigDecimal saldoAvalable;
    private Socio objSocio;
    private SolicitudPrestamo objSolicitudPrestamo;

    public DetalleSolicitudPrestamoSocio(long id, boolean garante, BigDecimal ingresos, BigDecimal egresos, BigDecimal otrosIngresos, BigDecimal saldoAvalable, Socio objSocio, SolicitudPrestamo objSolicitudPrestamo) {
        this.id = id;
        this.garante = garante;
        this.ingresos = ingresos;
        this.egresos = egresos;
        this.otrosIngresos = otrosIngresos;
        this.saldoAvalable = saldoAvalable;
        this.objSocio = objSocio;
        this.objSolicitudPrestamo = objSolicitudPrestamo;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isGarante() {
        return garante;
    }

    public void setGarante(boolean garante) {
        this.garante = garante;
    }

    public BigDecimal getIngresos() {
        return ingresos;
    }

    public void setIngresos(BigDecimal ingresos) {
        this.ingresos = ingresos;
    }

    public BigDecimal getEgresos() {
        return egresos;
    }

    public void setEgresos(BigDecimal egresos) {
        this.egresos = egresos;
    }

    public BigDecimal getOtrosIngresos() {
        return otrosIngresos;
    }

    public void setOtrosIngresos(BigDecimal otrosIngresos) {
        this.otrosIngresos = otrosIngresos;
    }

    public BigDecimal getSaldoAvalable() {
        return saldoAvalable;
    }

    public void setSaldoAvalable(BigDecimal saldoAvalable) {
        this.saldoAvalable = saldoAvalable;
    }

    public Socio getObjSocio() {
        return objSocio;
    }

    public void setObjSocio(Socio objSocio) {
        this.objSocio = objSocio;
    }

    public SolicitudPrestamo getObjSolicitudPrestamo() {
        return objSolicitudPrestamo;
    }

    public void setObjSolicitudPrestamo(SolicitudPrestamo objSolicitudPrestamo) {
        this.objSolicitudPrestamo = objSolicitudPrestamo;
    }
}
