package com.smartech.sistemacooperativa.dominio;

/**
 *
 * @author Smartech
 */
public class Concepto {
    
    public static final int CONCEPTO_APORTES = 1;
    public static final int CONCEPTO_DEPOSITOS = 2;
    public static final int CONCEPTO_TRANSFERENCIAS = 3;
    public static final int CONCEPTO_RETIROS_CUENTA = 4;
    public static final int CONCEPTO_CUOTAS = 5;
    public static final int CONCEPTO_RETIROS_SOCIO = 6;
    public static final int CONCEPTO_INGRESO_CAJA_ADMINISTRATIVO = 7;
    public static final int CONCEPTO_EGRESO_CAJA_ADMINISTRATIVO = 8;
    public static final int CONCEPTO_SOLICITUD_PRESTAMO = 9;
    
    private int id;
    private String nombre;
    private boolean movimiento;

    public Concepto() {
        this.id = 0;
        this.nombre = "";
    }
    
    public Concepto(int id, String nombre, boolean movimiento) {
        this.id = id;
        this.nombre = nombre;
        this.movimiento = movimiento;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }    

    public boolean isMovimiento() {
        return movimiento;
    }

    public void setMovimiento(boolean movimiento) {
        this.movimiento = movimiento;
    }
}
