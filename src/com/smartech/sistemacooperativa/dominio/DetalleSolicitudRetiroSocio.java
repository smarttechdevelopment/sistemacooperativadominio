/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.dominio;

/**
 *
 * @author Smartech
 */
public class DetalleSolicitudRetiroSocio {
    
    private long id;
    private Socio objSocio;
    private Usuario objUsuario;
    private boolean aprueba;

    public DetalleSolicitudRetiroSocio(long id, Socio objSocio, Usuario objUsuario, boolean aprueba) {
        this.id = id;
        this.objSocio = objSocio;
        this.objUsuario = objUsuario;
        this.aprueba = aprueba;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Socio getObjSocio() {
        return objSocio;
    }

    public void setObjSocio(Socio objSocio) {
        this.objSocio = objSocio;
    }

    public Usuario getObjUsuario() {
        return objUsuario;
    }

    public void setObjUsuario(Usuario objUsuario) {
        this.objUsuario = objUsuario;
    }

    public boolean isAprueba() {
        return aprueba;
    }

    public void setAprueba(boolean aprueba) {
        boolean tienePermiso = false;
        if(this.getId() > 0){
            tienePermiso = this.objUsuario.getObjTipoUsuario().verificarPermiso(Permiso.PERMISO_DETALLE_SOLICITUD_RETIRO_SOCIO_ID, Permiso.NIVEL_MODIFICACION);
        }else{
            tienePermiso =this.objUsuario.getObjTipoUsuario().verificarPermiso(Permiso.PERMISO_DETALLE_SOLICITUD_RETIRO_SOCIO_ID, Permiso.NIVEL_ESCRITURA);
        }
        if(tienePermiso){
            this.aprueba = aprueba;
        }
    }
    
    
}
