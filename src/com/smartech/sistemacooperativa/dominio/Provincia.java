package com.smartech.sistemacooperativa.dominio;

import java.util.List;

/**
 *
 * @author Smartech
 */
public class Provincia {
    private int id;
    private String nombre;
    private Departamento objDepartamento;
    private List<Distrito> lstDistritos;

    public Provincia(int id, String nombre, Departamento objDepartamento, List<Distrito> lstDistritos) {
        this.id = id;
        this.nombre = nombre;
        this.objDepartamento = objDepartamento;
        this.lstDistritos = lstDistritos;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre.toUpperCase();
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Departamento getObjDepartamento() {
        return objDepartamento;
    }

    public void setObjDepartamento(Departamento objDepartamento) {
        this.objDepartamento = objDepartamento;
    }

    public List<Distrito> getLstDistritos() {
        return lstDistritos;
    }

    public void setLstDistritos(List<Distrito> lstDistritos) {
        this.lstDistritos = lstDistritos;
    }
}
