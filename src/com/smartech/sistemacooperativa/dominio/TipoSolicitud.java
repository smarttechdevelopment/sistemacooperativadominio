package com.smartech.sistemacooperativa.dominio;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class TipoSolicitud {
    private int id;
    private String nombre;
    private boolean estado;
    private List<DetalleTipoSolicitudDocumento> lstDetalleTipoSolicitudDocumento;
    private List<TipoUsuario> lstTiposUsuario;

    public TipoSolicitud(int id, String nombre, boolean estado, List<DetalleTipoSolicitudDocumento> lstDetalleTipoSolicitudDocumento, List<TipoUsuario> lstTiposUsuarios) {
        this.id = id;
        this.nombre = nombre;
        this.estado = estado;
        this.lstDetalleTipoSolicitudDocumento = lstDetalleTipoSolicitudDocumento;
        this.lstTiposUsuario = lstTiposUsuarios;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre.toUpperCase();
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public List<DetalleTipoSolicitudDocumento> getLstDetalleTipoSolicitudDocumento() {
        return lstDetalleTipoSolicitudDocumento;
    }

    public void setLstDetalleTipoSolicitudDocumento(List<DetalleTipoSolicitudDocumento> lstDetalleTipoSolicitudDocumento) {
        this.lstDetalleTipoSolicitudDocumento = lstDetalleTipoSolicitudDocumento;
    }

    public List<TipoUsuario> getLstTiposUsuario() {
        return lstTiposUsuario;
    }

    public void setLstTiposUsuario(List<TipoUsuario> lstTiposUsuario) {
        this.lstTiposUsuario = lstTiposUsuario;
    }
    
    public List<DetalleDocumentoSolicitud> generarDetalleDocumentoSolicitud(Usuario objUsuario) {
        List<DetalleDocumentoSolicitud> lstDetalleDocumentoSolicitud = new ArrayList<>();

        for (DetalleTipoSolicitudDocumento objDetalleTipoSolicitudDocumento : this.getLstDetalleTipoSolicitudDocumento()) {
            DetalleDocumentoSolicitud objDetalleDocumentoSolicitud = new DetalleDocumentoSolicitud(
                    0,
                    null, //fechaentrega
                    objDetalleTipoSolicitudDocumento.getObjDocumento(),
                    "", //ruta
                    null, //representacion
                    null, //objsolicitud
                    false, //aprobado
                    null, //fechamodificacion
                    null, //fechasolicitud
                    objUsuario
            );
            lstDetalleDocumentoSolicitud.add(objDetalleDocumentoSolicitud);
        }

        return lstDetalleDocumentoSolicitud;
    }    
            
    public List<Aprobacion> generarAprobaciones(){
        List<Aprobacion> lstAprobaciones = new ArrayList<>();
        
        for(TipoUsuario objTipoUsuario : this.lstTiposUsuario){
            lstAprobaciones.add(new Aprobacion(
                    0, 
                    false,
                    null,
                    objTipoUsuario,
                    null));
        }
        
        return lstAprobaciones;
    }
    
    public static void updateAprobacion(TipoUsuario objTipoUsuario, List<Aprobacion> lstAprobaciones, boolean aprobado){
        for(Aprobacion objAprobacion : lstAprobaciones){
            if(objTipoUsuario.getId() == objAprobacion.getObjTipoUsuario().getId()){
                objAprobacion.setAprobado(aprobado);
            }
        }
    }
}
