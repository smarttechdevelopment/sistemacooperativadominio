package com.smartech.sistemacooperativa.dominio;

import com.smartech.sistemacooperativa.util.generics.DateUtil;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class Retiro implements ITransaccion {

    public static Moneda objMonedaBase = new Moneda(0, "Sol", "PEN", "S/", new BigDecimal(1), true);
    private static boolean permiteConvertirMoneda = false;
            
    private long id;
    private BigDecimal monto;
    private Timestamp fechaRegistro;
    private Timestamp fechaModificacion;
    private boolean estado;
    private Usuario objUsuario;
    private Cuenta objCuenta;
    private String acreedor;
    private List<Pago> lstPagos;
    private boolean habilitado;
    private boolean pagado;

    public Retiro(long id, BigDecimal monto, Timestamp fechaRegistro, Timestamp fechaModificacion, boolean estado, Usuario objUsuario, Cuenta objCuenta, String acreedor, List<Pago> lstPagos, boolean habilitado, boolean pagado) {
        this.id = id;
        this.monto = monto;
        this.fechaRegistro = fechaRegistro;
        this.fechaModificacion = fechaModificacion;
        this.estado = estado;
        this.objUsuario = objUsuario;
        this.objCuenta = objCuenta;
        this.acreedor = acreedor;
        this.lstPagos = lstPagos;
        this.habilitado = habilitado;
        this.pagado = pagado;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public Timestamp getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Timestamp fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Timestamp getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Timestamp fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public Usuario getObjUsuario() {
        return objUsuario;
    }

    public void setObjUsuario(Usuario objUsuario) {
        this.objUsuario = objUsuario;
    }

    public Cuenta getObjCuenta() {
        return objCuenta;
    }

    public void setObjCuenta(Cuenta objCuenta) {
        this.objCuenta = objCuenta;
    }

    public String getAcreedor() {
        return acreedor;
    }

    public void setAcreedor(String acreedor) {
        this.acreedor = acreedor;
    }

    public List<Pago> getLstPagos() {
        return lstPagos;
    }

    public void setLstPagos(List<Pago> lstPagos) {
        this.lstPagos = lstPagos;
    }

    public boolean isHabilitado() {
        return habilitado;
    }

    public void setHabilitado(boolean habilitado) {
        this.habilitado = habilitado;
    }

    public boolean isPagado() {
        return pagado;
    }

    public void setPagado(boolean pagado) {
        this.pagado = pagado;
    }

    public boolean evaluarMontoRetiroYCuenta(){
        
        return this.getObjCuenta().getSaldo().compareTo(this.getMonto()) > -1;
    }

    public static boolean advertirMontoRetiro(BigDecimal monto, Moneda objMoneda, TipoUsuario objTipoUsuario) {
        boolean resultado = true;

        if (objMonedaBase != null) {
            if(objMonedaBase.getId() >= 0){
                if (objMoneda.getAbreviatura().equals(objMonedaBase.getAbreviatura())) {
                    if (monto.compareTo(objTipoUsuario.getMontoAdvertenciaTransaccion()) < 0) {
                        resultado = false;
                    }
                } else if (permiteConvertirMoneda && objMoneda.getValorPEN(monto).compareTo(objMonedaBase.getValorPEN(objTipoUsuario.getMontoAdvertenciaTransaccion())) < 0) {
                    resultado = false;
                }
            }
        }

        return resultado;
    }
    
    public boolean verificarMontoRetiroYPagos(){        
        return this.getMontoTotalPagos().compareTo(this.monto) >= 0;
    }
    
    public BigDecimal getMontoTotalPagos(){        
        BigDecimal montoTotal = new BigDecimal(-1);
        
        if(!this.getLstPagos().isEmpty()){
            montoTotal = BigDecimal.ZERO;
            for(Pago objPago : this.getLstPagos()){
                montoTotal = montoTotal.add(objPago.getMonto());
            }
        }
        
        return montoTotal;
    }
    
    public boolean actualizarCuenta(){        
        if(this.verificarMontoRetiroYPagos() && this.evaluarMontoRetiroYCuenta()){
            
            this.getObjCuenta().setSaldo(this.getObjCuenta().getSaldo().subtract(this.monto));
            this.setFechaModificacion(DateUtil.currentTimestamp());
            return true;
        }
        return false;
    }

    @Override
    public String getDescripcionTransaccion() {
        return "Retiro";
    }

    @Override
    public boolean isIngreso() {
        return false;
    }
}
