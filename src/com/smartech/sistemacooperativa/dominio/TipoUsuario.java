package com.smartech.sistemacooperativa.dominio;

import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author Usuario
 */
public class TipoUsuario {
    
    public static int TIPO_ADMINISTRADOR = 1;
    public static int TIPO_PRESIDENTE_CONSEJO_ADMINISTRACION = 2;
    public static int TIPO_GERENTE_COOPERATIVA = 3;
    public static int TIPO_VICEPRESIDENTE_CONSEJO_ADMINISTRACION = 4;
    public static int TIPO_CAJERO = 5;
    public static int TIPO_SOCIO = 6;
    
    private int id;
    private String nombre;
    private int tipoPersona;
    private int nivel;
    private boolean accesocaja;
    private BigDecimal montoAdvertenciaTransaccion;
    private List<Permiso> lstPermisos;
    private List<Acceso> lstAccesos;

    public TipoUsuario(int id, String nombre, int tipoPersona, int nivel, boolean accesocaja, BigDecimal montoAdvertenciaTransaccion, List<Permiso> lstPermisos, List<Acceso> lstAccesos) {
        this.id = id;
        this.nombre = nombre;
        this.tipoPersona = tipoPersona;
        this.nivel = nivel;
        this.accesocaja = accesocaja;
        this.montoAdvertenciaTransaccion = montoAdvertenciaTransaccion;
        this.lstPermisos = lstPermisos;
        this.lstAccesos = lstAccesos;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre.toUpperCase();
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(int tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public boolean isAccesocaja() {
        return accesocaja;
    }

    public void setAccesocaja(boolean accesocaja) {
        this.accesocaja = accesocaja;
    }

    public BigDecimal getMontoAdvertenciaTransaccion() {
        return montoAdvertenciaTransaccion;
    }

    public void setMontoAdvertenciaTransaccion(BigDecimal montoAdvertenciaTransaccion) {
        this.montoAdvertenciaTransaccion = montoAdvertenciaTransaccion;
    }

    public List<Permiso> getLstPermisos() {
        return lstPermisos;
    }

    public void setLstPermisos(List<Permiso> lstPermisos) {
        this.lstPermisos = lstPermisos;
    }

    public List<Acceso> getLstAccesos() {
        return lstAccesos;
    }

    public void setLstAccesos(List<Acceso> lstAccesos) {
        this.lstAccesos = lstAccesos;
    }
    
    public boolean verificarAcceso(String nombreAcceso){
        for(Acceso objAcceso : this.lstAccesos){
            if(objAcceso.getNombre().equals(nombreAcceso)){
                return true;
            }
        }
        
        return false;
    }
    
    public boolean verificarPermiso(String nombrePermiso, int nivelRequerido){
        for(Permiso objPermiso : this.lstPermisos){
            if(objPermiso.getNombre().equals(nombrePermiso)){
                return objPermiso.getNivel() >= nivelRequerido;                
            }
        }
        
        return false;
    }
    
    public boolean verificarPermiso(int idPermiso, int nivelRequerido){
        for(Permiso objPermiso : this.lstPermisos){
            if(objPermiso.getId() == idPermiso){
                if(objPermiso.getNivel() >= nivelRequerido){
                    return true;
                }else{
                    break;
                }                
            }
        }
        return false;
    }
}
