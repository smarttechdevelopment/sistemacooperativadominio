package com.smartech.sistemacooperativa.dominio;

import com.smartech.sistemacooperativa.util.generics.BigDecimalUtil;
import com.smartech.sistemacooperativa.util.generics.DateUtil;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class Prestamo {

    public static final int METODO_FRANCES = 0;
    public static final int METODO_ALEMAN = 1;

    private long id;
    private int cuotas;
    private BigDecimal monto;
    private BigDecimal tem;
    private BigDecimal ted;
    private Timestamp fechaRegistro;
    private Timestamp fechaModificacion;
    private boolean estado;
    private boolean pagado;
    private SolicitudPrestamo objSolicitudPrestamo;
    private boolean aprobado;
    private List<Cuota> lstCuotas;

    public Prestamo(long id, int cuotas, BigDecimal monto, Timestamp fechaRegistro, Timestamp fechaModificacion, boolean estado, boolean pagado, SolicitudPrestamo objSolicitudPrestamo, boolean aprobado) {
        this.id = id;
        this.cuotas = cuotas;
        this.monto = monto;
        this.tem = BigDecimal.ZERO;
        this.ted = BigDecimal.ZERO;
        this.fechaRegistro = fechaRegistro;
        this.fechaModificacion = fechaModificacion;
        this.estado = estado;
        this.pagado = pagado;
        this.objSolicitudPrestamo = objSolicitudPrestamo;
        this.aprobado = aprobado;
        this.lstCuotas = new ArrayList<>();
    }

    public Prestamo(long id, int cuotas, BigDecimal monto, BigDecimal tem, BigDecimal ted, Timestamp fechaRegistro, Timestamp fechaModificacion, boolean estado, boolean pagado, SolicitudPrestamo objSolicitudPrestamo, boolean aprobado) {
        this.id = id;
        this.cuotas = cuotas;
        this.monto = monto;
        this.tem = tem;
        this.ted = ted;
        this.fechaRegistro = fechaRegistro;
        this.fechaModificacion = fechaModificacion;
        this.estado = estado;
        this.pagado = pagado;
        this.objSolicitudPrestamo = objSolicitudPrestamo;
        this.aprobado = aprobado;
        this.lstCuotas = new ArrayList<>();
    }

    public Prestamo(long id, int cuotas, BigDecimal monto, BigDecimal tem, BigDecimal ted, Timestamp fechaRegistro, Timestamp fechaModificacion, boolean estado, boolean pagado, SolicitudPrestamo objSolicitudPrestamo, boolean aprobado, List<Cuota> lstCuotas) {
        this.id = id;
        this.cuotas = cuotas;
        this.monto = monto;
        this.tem = tem;
        this.ted = ted;
        this.fechaRegistro = fechaRegistro;
        this.fechaModificacion = fechaModificacion;
        this.estado = estado;
        this.pagado = pagado;
        this.objSolicitudPrestamo = objSolicitudPrestamo;
        this.aprobado = aprobado;
        this.lstCuotas = lstCuotas;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getCuotas() {
        return cuotas;
    }

    public void setCuotas(int cuotas) {
        this.cuotas = cuotas;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public BigDecimal getTem() {
        return tem;
    }

    public void setTem(BigDecimal tem) {
        this.tem = tem;
    }

    public BigDecimal getTed() {
        return ted;
    }

    public void setTed(BigDecimal ted) {
        this.ted = ted;
    }

    public Timestamp getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Timestamp fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Timestamp getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Timestamp fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public boolean isPagado() {
        return pagado;
    }

    public void setPagado(boolean pagado) {
        this.pagado = pagado;
    }

    public SolicitudPrestamo getObjSolicitudPrestamo() {
        return objSolicitudPrestamo;
    }

    public void setObjSolicitud(SolicitudPrestamo objSolicitudPrestamo) {
        this.objSolicitudPrestamo = objSolicitudPrestamo;
    }

    public boolean isAprobado() {
        return aprobado;
    }

    public void setAprobado(boolean aprobado) {
        this.aprobado = aprobado;
    }

    public List<Cuota> getLstCuotas() {
        return lstCuotas;
    }

    public void setLstCuotas(List<Cuota> lstCuotas) {
        this.lstCuotas = lstCuotas;
    }

    public Cuota generarCuota(int numero, BigDecimal monto, BigDecimal interes, BigDecimal amortizacion, BigDecimal deudaPendiente, Calendar fechaVencimiento) {
        return new Cuota(
                0,
                monto,
                numero,
                interes,
                amortizacion,
                BigDecimal.ZERO,
                deudaPendiente,
                DateUtil.getTimestamp(fechaVencimiento),
                null,
                false,
                DateUtil.currentTimestamp(),
                true,
                this.objSolicitudPrestamo == null ? null : this.objSolicitudPrestamo.getObjUsuario(),
                this);
    }

    public List<Cuota> generarCuotas(int metodo) {
        List<Cuota> lstCuotasGeneradas = new ArrayList<>();

        switch (metodo) {
            case METODO_FRANCES:
                lstCuotasGeneradas = this.generarCuotasMetodoFrances();
                break;

            case METODO_ALEMAN:
                lstCuotasGeneradas = this.generarCuotasMetodoAleman();
                break;
        }

        return lstCuotasGeneradas;
    }

    public List<Cuota> generarCuotas(int metodo, TasaInteres objTasaInteres, Date fechaCalculo) {
        List<Cuota> lstCuotasGeneradas = new ArrayList<>();

        switch (metodo) {
            case METODO_FRANCES:
                lstCuotasGeneradas = this.generarCuotasMetodoFrances(objTasaInteres, fechaCalculo);
                break;

            case METODO_ALEMAN:
                lstCuotasGeneradas = this.generarCuotasMetodoAleman(objTasaInteres, fechaCalculo);
                break;
        }

        return lstCuotasGeneradas;
    }

    public void actualizarInteresCuotas(int metodo, int cuotaInicial, int cuotaFinal, BigDecimal tasaInteres) {
        switch (metodo) {
            case METODO_FRANCES:

                break;

            case METODO_ALEMAN:
                this.actualizarInteresCuotasMetodoAleman(cuotaInicial, cuotaFinal, tasaInteres);
                break;
        }
    }

    private void actualizarInteresCuotasMetodoAleman(int cuotaInicial, int cuotaFinal, BigDecimal tasaInteres) {
        Cuota ultimaCuota = cuotaInicial > 0 ? this.lstCuotas.get(cuotaInicial - 1) : this.lstCuotas.get(0);

        BigDecimal saldo = new BigDecimal(BigInteger.ZERO);
        saldo = saldo.add(ultimaCuota.getDeudaPendiente());
        BigDecimal cuota = new BigDecimal(BigInteger.ZERO);
        BigDecimal interes = new BigDecimal(BigInteger.ZERO);
        BigDecimal amortizacion = ultimaCuota.getAmortizacion();

        for (int i = cuotaInicial; i < cuotaFinal; i++) {
            interes = this.objSolicitudPrestamo.getObjTasaInteres().getObjTipoInteres().isPlazoFijo() ? tasaInteres : saldo.multiply(tasaInteres);
            cuota = amortizacion.add(interes);
            saldo = saldo.subtract(amortizacion);

            Cuota objCuota = this.lstCuotas.get(i);
            objCuota.setInteres(interes);
            objCuota.setMonto(cuota);
        }
    }

    private List<Cuota> generarCuotasMetodoAleman() {
        List<Cuota> lstCuotasGeneradas = new ArrayList<>();

        this.tem = this.calcularTEM();

        Calendar fechaInicio = new GregorianCalendar();
        fechaInicio.setTime(new Date());
        Calendar fechaFin = new GregorianCalendar();

        BigDecimal saldo = new BigDecimal(BigInteger.ZERO);
        saldo = saldo.add(this.monto);
        BigDecimal cuota = new BigDecimal(BigInteger.ZERO);
        BigDecimal interes = new BigDecimal(BigInteger.ZERO);
        BigDecimal amortizacion = this.monto.divide(BigDecimal.valueOf(this.cuotas), 4, RoundingMode.HALF_UP);

        for (int i = 0; i < this.cuotas; i++) {
            interes = this.objSolicitudPrestamo.getObjTasaInteres().getObjTipoInteres().isPlazoFijo() ? this.objSolicitudPrestamo.getObjTasaInteres().getMontoPlazoFijo() : saldo.multiply(this.tem);
            cuota = amortizacion.add(interes);
            saldo = saldo.subtract(amortizacion);

            fechaFin.setTime(fechaInicio.getTime());
            fechaFin.add(Calendar.DAY_OF_MONTH, this.objSolicitudPrestamo.getObjTasaInteres().getObjTipoInteres().getDiasPlazos());
            Cuota objCuota = this.generarCuota(i + 1, cuota, interes, amortizacion, saldo, fechaFin);
            lstCuotasGeneradas.add(objCuota);
            fechaInicio.setTime(fechaFin.getTime());
        }

        return lstCuotasGeneradas;
    }

    private List<Cuota> generarCuotasMetodoAleman(TasaInteres objTasaInteres, Date fechaCalculo) {
        List<Cuota> lstCuotasGeneradas = new ArrayList<>();

        this.tem = this.calcularTEM(objTasaInteres);

        Calendar fechaInicio = new GregorianCalendar();
        fechaInicio.setTime(fechaCalculo);
        Calendar fechaFin = new GregorianCalendar();

        BigDecimal saldo = new BigDecimal(BigInteger.ZERO);
        saldo = saldo.add(this.monto);
        BigDecimal cuota = new BigDecimal(BigInteger.ZERO);
        BigDecimal interes = new BigDecimal(BigInteger.ZERO);
        BigDecimal amortizacion = this.monto.divide(BigDecimal.valueOf(this.cuotas), 4, RoundingMode.HALF_UP);

        for (int i = 0; i < this.cuotas; i++) {
            interes = objTasaInteres.getObjTipoInteres().isPlazoFijo() ? objTasaInteres.getMontoPlazoFijo() : saldo.multiply(this.tem);
            cuota = amortizacion.add(interes);
            saldo = saldo.subtract(amortizacion);

            fechaFin.setTime(fechaInicio.getTime());
            fechaFin.add(Calendar.DAY_OF_MONTH, objTasaInteres.getObjTipoInteres().getDiasPlazos());
            Cuota objCuota = this.generarCuota(i + 1, cuota, interes, amortizacion, saldo, fechaFin);
            lstCuotasGeneradas.add(objCuota);
            fechaInicio.setTime(fechaFin.getTime());
        }

        return lstCuotasGeneradas;
    }

    private List<Cuota> generarCuotasMetodoFrances() {
        List<Cuota> lstCuotasGeneradas = new ArrayList<>();

        this.tem = this.calcularTEM();
        this.ted = this.calcularTED();

        BigDecimal temPotencia = (BigDecimal.valueOf(1).add(this.tem)).pow(this.cuotas);

        BigDecimal numerador = this.tem.multiply(temPotencia);
        BigDecimal denominador = temPotencia.subtract(BigDecimal.valueOf(1));

        BigDecimal cuota = this.monto.multiply((numerador.divide(denominador, 4, RoundingMode.HALF_UP)));

        BigDecimal interes = new BigDecimal(BigInteger.ZERO);
        BigDecimal amortizacion = new BigDecimal(BigInteger.ZERO);
        BigDecimal saldo = new BigDecimal(BigInteger.ZERO);
        saldo = saldo.add(this.monto);
        Calendar fechaInicio = new GregorianCalendar();
        fechaInicio.setTime(new Date());
        Calendar fechaFin = new GregorianCalendar();
        for (int i = 0; i < this.cuotas; i++) {
            interes = this.objSolicitudPrestamo.getObjTasaInteres().getObjTipoInteres().isPlazoFijo() ? this.objSolicitudPrestamo.getObjTasaInteres().getMontoPlazoFijo() : saldo.multiply(this.tem);
            amortizacion = cuota.subtract(interes);
            saldo = saldo.subtract(amortizacion);

            fechaFin.setTime(fechaInicio.getTime());
            fechaFin.add(Calendar.DAY_OF_MONTH, this.objSolicitudPrestamo.getObjTasaInteres().getObjTipoInteres().getDiasPlazos());
            Cuota objCuota = this.generarCuota(i + 1, cuota, interes, amortizacion, saldo, fechaFin);
            lstCuotasGeneradas.add(objCuota);
            fechaInicio.setTime(fechaFin.getTime());
        }

        return lstCuotasGeneradas;
    }

    private List<Cuota> generarCuotasMetodoFrances(TasaInteres objTasaInteres, Date fechaCalculo) {
        List<Cuota> lstCuotasGeneradas = new ArrayList<>();

        this.tem = this.calcularTEM(objTasaInteres);
        this.ted = this.calcularTED(objTasaInteres);

        BigDecimal temPotencia = (BigDecimal.valueOf(1).add(this.tem)).pow(this.cuotas);

        BigDecimal numerador = this.tem.multiply(temPotencia);
        BigDecimal denominador = temPotencia.subtract(BigDecimal.valueOf(1));

        BigDecimal cuota = this.monto.multiply((numerador.divide(denominador, 4, RoundingMode.HALF_UP)));

        BigDecimal interes = new BigDecimal(BigInteger.ZERO);
        BigDecimal amortizacion = new BigDecimal(BigInteger.ZERO);
        BigDecimal saldo = new BigDecimal(BigInteger.ZERO);
        saldo = saldo.add(this.monto);
        Calendar fechaInicio = new GregorianCalendar();
        fechaInicio.setTime(fechaCalculo);
        Calendar fechaFin = new GregorianCalendar();
        for (int i = 0; i < this.cuotas; i++) {
            interes = objTasaInteres.getObjTipoInteres().isPlazoFijo() ? objTasaInteres.getMontoPlazoFijo() : saldo.multiply(this.tem);
            amortizacion = cuota.subtract(interes);
            saldo = saldo.subtract(amortizacion);

            fechaFin.setTime(fechaInicio.getTime());
            fechaFin.add(Calendar.DAY_OF_MONTH, objTasaInteres.getObjTipoInteres().getDiasPlazos());
            Cuota objCuota = this.generarCuota(i + 1, cuota, interes, amortizacion, saldo, fechaFin);
            lstCuotasGeneradas.add(objCuota);
            fechaInicio.setTime(fechaFin.getTime());
        }

        return lstCuotasGeneradas;
    }

    public void ajustarCuotasMensual(int diaPago) {
        Calendar fechaCuota = Calendar.getInstance();
        Calendar fechaCalculo;
        Cuota primeraCuota;
        BigDecimal interesCompensatorio;

        this.lstCuotas = this.generarCuotas(Prestamo.METODO_ALEMAN);
        primeraCuota = this.lstCuotas.get(0);

        if (diaPago > 0 && diaPago <= DateUtil.getCalendar(DateUtil.lastMonthTime(primeraCuota.getFechaVencimiento())).get(Calendar.DAY_OF_MONTH)) {
            // La idea es que las cuotas se modifiquen de acuerdo al dia proyectado el proximo mes 
            // Si el diaPago (dia de preferencia, el dia del mes actualizado) es mayor al dia de pago calculado al proximo mes entonces adiciona un monto de interes
            // No se toma en cuenta cuando sucede lo contrario
            fechaCalculo = DateUtil.getCalendar(primeraCuota.getFechaVencimiento());
            fechaCalculo.set(Calendar.DAY_OF_MONTH, diaPago);

            //<condicion agregada> Agrega monto de interes compensatorio si la fecha a pagar es mayor a 30 dias aproximadamente
            if (primeraCuota.getFechaVencimiento().before(DateUtil.getTimestamp(fechaCalculo))) {
                interesCompensatorio = primeraCuota.calcularInteresCompensatorioVencido(DateUtil.getDate(DateUtil.getTimestamp(fechaCalculo)));
                primeraCuota.setInteres(primeraCuota.getInteres().add(interesCompensatorio));
                primeraCuota.setMonto(primeraCuota.getMonto().add(interesCompensatorio));
            }
            //</condicion agregada>

            for (Cuota objCuota : this.lstCuotas) {
                fechaCuota.setTimeInMillis(objCuota.getFechaVencimiento().getTime());
                if (fechaCuota.get(Calendar.DAY_OF_MONTH) != diaPago) {
                    if (diaPago > fechaCuota.getActualMaximum(Calendar.DAY_OF_MONTH)) {
                        DateUtil.lastMonthTime(fechaCuota);
                    } else {
                        fechaCuota.set(Calendar.DAY_OF_MONTH, diaPago);
                    }
                    objCuota.setFechaVencimiento(DateUtil.getTimestamp(fechaCuota));
                }
            }

        }

    }

    public BigDecimal calcularTEM() {
        BigDecimal temValue = new BigDecimal(BigInteger.ZERO);

        try {
            BigDecimal potencia = BigDecimal.valueOf(this.objSolicitudPrestamo.getObjTasaInteres().getObjTipoInteres().getDiasPlazos()).divide(BigDecimal.valueOf(360), 4, RoundingMode.HALF_UP);
            BigDecimal base = BigDecimal.ONE.add(this.objSolicitudPrestamo.getObjTasaInteres().getTea());

            BigDecimal temp = BigDecimalUtil.round(BigDecimal.valueOf(Math.pow(base.doubleValue(), potencia.doubleValue())), 4);
            temValue = temp.subtract(BigDecimal.ONE);
            temValue = temValue.multiply(BigDecimal.valueOf(10));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return temValue;
    }

    public BigDecimal calcularTED() {
        BigDecimal tedValue = new BigDecimal(BigInteger.ZERO);

        try {
            BigDecimal potencia = BigDecimal.ONE.divide(BigDecimal.valueOf(360), 4, RoundingMode.HALF_UP);
            BigDecimal base = BigDecimal.ONE.add(this.objSolicitudPrestamo.getObjTasaInteres().getTea());

            tedValue = BigDecimal.valueOf(Math.pow(base.doubleValue(), potencia.doubleValue())).subtract(BigDecimal.ONE);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tedValue;
    }

    public BigDecimal calcularTEM(TasaInteres objTasaInteres) {
        BigDecimal temValue = new BigDecimal(BigInteger.ZERO);

        try {
            BigDecimal potencia = BigDecimal.valueOf(objTasaInteres.getObjTipoInteres().getDiasPlazos()).divide(BigDecimal.valueOf(360), 3, RoundingMode.HALF_UP);
            BigDecimal base = BigDecimal.ONE.add(objTasaInteres.getTea());

            BigDecimal temp = BigDecimalUtil.round(BigDecimal.valueOf(Math.pow(base.doubleValue(), potencia.doubleValue())), 4);
            temValue = temp.subtract(BigDecimal.ONE);
            temValue = temValue.multiply(BigDecimal.valueOf(10));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return temValue;
    }

    public BigDecimal calcularTED(TasaInteres objTasaInteres) {
        BigDecimal tedValue = new BigDecimal(BigInteger.ZERO);

        try {
            BigDecimal potencia = BigDecimal.ONE.divide(BigDecimal.valueOf(360), 4, RoundingMode.HALF_UP);
            BigDecimal base = BigDecimal.ONE.add(objTasaInteres.getTea());

            tedValue = BigDecimal.valueOf(Math.pow(base.doubleValue(), potencia.doubleValue())).subtract(BigDecimal.ONE);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tedValue;
    }

    public void setPagado() {
        for (Cuota objCuota : this.lstCuotas) {
            if (!objCuota.isPagado()) {
                this.pagado = false;
                return;
            }
        }

        this.pagado = true;
    }

    public List<Cuota> getCuotasRetrasadas(Date fecha) {
        List<Cuota> lstCuotasResult = new ArrayList<>();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha);

        Calendar calendarCuota = Calendar.getInstance();
        for (Cuota objCuota : this.lstCuotas) {
            calendarCuota.setTimeInMillis(objCuota.getFechaVencimiento().getTime());
            if (calendar.after(calendarCuota) && !objCuota.isPagado()) {
                lstCuotasResult.add(objCuota);
            }
        }

        return lstCuotasResult;
    }

    public List<Cuota> getCuotasVencidas() {
        List<Cuota> lstCuotasResult = new ArrayList<>();

        Calendar calendarVencimientoCuota = Calendar.getInstance();
        Calendar calendarPagoCuota = Calendar.getInstance();
        for (Cuota objCuota : this.lstCuotas) {
            if (objCuota.isPagado()) {
                calendarVencimientoCuota.setTimeInMillis(objCuota.getFechaVencimiento().getTime());
                calendarPagoCuota.setTimeInMillis(objCuota.getFechaPago().getTime());
                if (calendarPagoCuota.after(calendarVencimientoCuota)) {
                    lstCuotasResult.add(objCuota);
                }
            }
        }

        return lstCuotasResult;
    }

    public List<Cuota> getCuotasPendientes() {
        List<Cuota> lstCuotasResult = new ArrayList<>();

        for (Cuota objCuota : this.lstCuotas) {
            if (!objCuota.isPagado()) {
                lstCuotasResult.add(objCuota);
            }
        }

        return lstCuotasResult;
    }

    public void updateMontosCuotas(Date fechaCalculo) {
        for (Cuota objCuota : this.lstCuotas) {
            objCuota.updateMontos(fechaCalculo);
        }
    }

    public BigDecimal calcularDeudaPendiente(Date fechaCalculo) {
        BigDecimal deudaPendiente = new BigDecimal(BigInteger.ZERO);

        List<Cuota> lstCuotasPendientes = this.getCuotasPendientes();
        for (Cuota objCuota : lstCuotasPendientes) {
            deudaPendiente = deudaPendiente.add(objCuota.calcularMonto(fechaCalculo));
        }

        return deudaPendiente;
    }
}
